export default {
  // 'common' (multi-tenant gateway) or Azure AD Tenant ID
  tenant: 'b27feeac-9dd2-4536-b19d-fe9b535fc1d8',

  // Application ID
  clientId: '7a995e62-29af-46d6-9528-aee20acf5864',

  // Host URI
  redirectUri: 'http://localhost:8080',

  cacheLocation: 'localStorage',

  // Set this to true for authentication on startup
  requireAuthOnInitialize: true,

  // Mapping for internal goups to AD goups
  mapping: {
    groups: {
      attributesManagement: {
        level1: 'everyone',
        level2: '',
        level3: ''
      },
      PublishFromList: {
        level1: 'everyone',
        level2: '',
        level3: ''
      },
      QueueSplittingTool: {
        level1: 'everyone',
        level2: '',
        level3: ''
      },
      neutraldataManagement: {
        level1: 'everyone',
        level2: '',
        level3: ''
      },
      checkAndConnect: {
        level1: 'everyone',
        level2: '',
        level3: ''
      },
      SynchronizeAndSort: {
        level1: 'everyone',
        level2: '',
        level3: ''
      }
    }
  }
}
