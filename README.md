# GIA Windchill Apps Frontend

## Desktop View
![Desktop_Main_Page](./docs/Desktop_Main_Page.jpg "Desktop Frontend Apps")

## Mobile View
![Mobile_Main_Page](./docs/Mobile_Main_Page.jpg "Mobile Frontend Apps")

An example (for demonstration purposes only, no connecting Windchill) can be found under https://hippenpower.gitlab.io/giawindchillapps/#/en

Login credentials:
username: ANYTHING_YOU_WANT
password: test

## Installation

To compile the webpage, you need to install [Nodejs](https://nodejs.org/) (Version v10.16.0 or higher)

Clone entire repository

First edit/create File (./frontend/env.production.local) and add/edit line
according to the new url

``` bash
VUE_APP_BASE_URL=https://localhost:443
VUE_APP_API_URL=/Windchill/ext/gia/api/v1/
```

then install dependencies. Open Console within folder ./frontend:

```
npm install
```

and then complie the source code whith

```
npm run build
```
check index.html in build folder of vadility (endings tags must be set for apache)

put compiled code into (./dist/) => (WINDCHILL_BASE/HTTPServer/htdocs/gia/)


### First Login
If the webpage is installed the first time, you can log on with user "wcadmin" or "Administrator" 
and then setup other users!

https://www.ptc.com/en/support/article/CS298908?&language=en&posno=1&q=authResAdditions&source=search

## Authors

* **Matthias Hippen** - *Initial work and maintainer*

## Built and Tested With

``` json
{ "giawindchillapps": "0.1.0",
  "npm": "6.13.4",
  "ares": "1.15.0",
  "brotli": "1.0.7",
  "cldr": "35.1",
  "http_parser": "2.8.0",
  "icu": "64.2",
  "modules": "64",
  "napi": "4",
  "nghttp2": "1.34.0",
  "node": "10.16.0",
  "openssl": "1.1.1b",
  "tz": "2019a",
  "unicode": "12.1",
  "uv": "1.28.0",
  "v82": "6.8.275.32-node.52",
  "zlib": "1.2.11"
}
  ```