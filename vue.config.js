module.exports = {
  // mode: 'production',
  chainWebpack: config => {
    config.module
      .rule('raw')
      .test(/\.txt$/)
      .use('raw-loader')
		  .loader('raw-loader')
		  .end()
	  .use('other-loader')
      .loader('other-loader')
      .end()
  },
  runtimeCompiler: true,
  devServer: {
    proxy: 'http://gial629-va'

  },
  // when production => use '/gia/'
  publicPath: process.env.NODE_ENV === 'production'
    ? process.env.VUE_APP_FRONTEND_EXTENSION
    : '/'
}
