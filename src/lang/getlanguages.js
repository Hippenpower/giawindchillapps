function getInstalledLanguages () {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = []
  locales.keys().forEach(key => {
    var langFile = require('./locales' + key.substring(1, key.length))
    messages.push({ code_2: key.substring(2, key.indexOf('.json')), name: langFile.language })
  })
  return messages
}

export default getInstalledLanguages
