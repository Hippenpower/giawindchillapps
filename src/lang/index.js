import VueI18n from 'vue-i18n'
import Vue from 'vue'
import en from './locales/en.json'
Vue.use(VueI18n)

const i18n = new VueI18n({
  /** 默认值 */
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en }
})

export default i18n
