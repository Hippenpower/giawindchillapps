import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'
import Login from '../views/LoginBase'
import Logout from '../views/Logout'
import { userService } from '../services'

// import languages from '../lang/getlanguages'
import GiaApps from '../json_files/gia_apps_products'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/en'
  },
  {
    path: '/:lang',
    component: {
      template: '<router-view></router-view>'
    },
    beforeEnter (to, from, next) {
      /*
      const lang = store.state.settings.user.language
      const langPath = to.params.lang
      console.log('lang: ' + lang + ', langPath: ' + langPath)
      if (lang !== langPath) {
        var languageCodes = languages().reduce((acc, obj) => {
          if (!Array.isArray(acc)) {
            acc = []
          }
          acc.push(obj.code_2)
          return acc
        }, {})
        console.log(languageCodes.toString() + ', lang: ' + langPath + ', is included: ' + languageCodes.includes(langPath))
        if (!languageCodes.includes(langPath)) {
          return next({ path: 'en', params: { lang: 'en' } })
        } else {
          // store.dispatch('setUserSettings', { user: { language: langPath }, force: false })
          return next({ path: '/:' + lang, params: { lang: lang } })
        }
      } else {
        next()
      }
      */

      const loggedIn = localStorage.getItem('user')

      // automaticly login when visiting site
      if (!store.state.loggedIn && loggedIn) {
        store.dispatch('setPerfomingLogging', true)
        const localUser = JSON.parse(loggedIn)
        const auth = window.atob(localUser.cred.substring(localUser.cred.indexOf('Basic ') + 'Basic '.length, localUser.cred.length))
        const username = auth.substring(0, auth.indexOf(':'))
        const password = auth.substring(auth.indexOf(':') + 1, auth.length)
        userService.login(username, password, localUser.remember)
          .finally(() => store.dispatch('setPerfomingLogging', false))
      }

      next()
    },
    children: [
      {
        path: '',
        name: 'Home',
        component: Home
        // component: () => import(/* webpackChunkName: "home" */ '../views/ConfigurationTabs.vue')
      },
      {
        path: 'login',
        name: 'Login',
        component: Login
        // component: () => import(/* webpackChunkName: "login" */ '../views/ConfigurationTabs.vue')
      },
      {
        path: 'logout',
        name: 'Logout',
        component: Logout
        // component: () => import(/* webpackChunkName: "logout" */ '../views/ConfigurationTabs.vue')
      },
      {
        path: 'licensevalidation',
        name: 'LicenseValidation',
        component: () => import(/* webpackChunkName: "logout" */ '../components/LicenseValidator.vue')
      },
      {
        path: 'version',
        name: 'Version',
        component: () => import(/* webpackChunkName: "home" */ '../views/Version.vue')
      },
      {
        path: 'logs',
        name: 'Logs',
        component: () => import(/* webpackChunkName: "home" */ '../views/GlobalLogHandling.vue')
      },
      {
        path: 'config',
        name: 'config',
        component: () => import(/* webpackChunkName: "config" */ '../views/ConfigurationTabs.vue'),
        children: [
          {
            path: 'useradmin',
            name: GiaApps.usermanagement.routername.config,
            component: () => import(/* webpackChunkName: "useradmin" */ '../views/config/UserManagement')
          },
          {
            path: 'neutraldata',
            name: GiaApps.neutraldata.routername.config,
            component: () => import(/* webpackChunkName: "neutraldata" */ '../views/config/Neutraldata')
          },
          {
            path: 'publishqueuemanager',
            name: GiaApps.publishqueuemanager.routername.config,
            component: () => import(/* webpackChunkName: "publishqueuemanager" */ '../views/config/PublishQueueManager')
          },
          {
            path: 'customattributes',
            name: GiaApps.customattributes.routername.config,
            component: () => import(/* webpackChunkName: "publishqueuemanager" */ '../views/config/CustomAttributes')
          },
          {
            path: 'publishfromlist',
            name: GiaApps.publishfromlist.routername.config,
            component: () => import(/* webpackChunkName: "publishfromlist" */ '../views/config/PublishFromList')
          },
          {
            path: 'sapchecker',
            name: GiaApps.sapchecker.routername.config,
            component: () => import(/* webpackChunkName: "publishfromlist" */ '../views/config/SapChecker')
          },
          {
            path: 'publishmonitor',
            name: GiaApps.publishmonitor.routername.config,
            component: () => import(/* webpackChunkName: "publishfromlist" */ '../views/config/PublishMonitor')
          },
          {
            path: 'synchronizer',
            name: GiaApps.synchronizer.routername.config,
            component: () => import(/* webpackChunkName: "publishfromlist" */ '../views/config/Synchronizer')
          }
        ]
      },
      {
        path: '/docs',
        name: 'docs',
        component: () => import(/* webpackChunkName: "configuration" */ '../views/ConfigurationTabs.vue'),
        children: [
          {
            path: 'useradmin',
            name: 'docsuseradmin'
            // component: () => import(/* webpackChunkName: "docsuseradmin" */ '../components/ShowDocs.vue')
          },
          {
            path: 'neutraldata',
            name: 'docsneutraldata'
            // component: () => import(/* webpackChunkName: "docsuseradmin" */ '../components/ShowDocs.vue')
          }
        ]
      },
      // otherwise redirect to home
      { path: '*', redirect: '/' }
    ]
  }
]

const router = new VueRouter({
  mode: process.env.VUE_APP_USE_SSO === 'TRUE' ? 'history' : 'hash',
  base: process.env.NODE_ENV === 'production' ? process.env.VUE_APP_FRONTEND_EXTENSION : '/',
  routes
})

// ignore duplicated navigation error (https://stackoverflow.com/questions/58634914/getting-navigation-duplicated-error-while-the-route-is-being-replaced-on-vue-rou)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
