import store from '@/store'

export const apiService = {
  postRequest,
  getRequest
}

const defaultConfig = {
  mode: 'cors', // no-cors, *cors, same-origin
  cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
  redirect: 'follow', // manual, *follow, error
  referrerPolicy: 'no-referrer', // no-referrer, *client
  credentials: 'include' // include, *same-origin, omit
}

function postRequest (url = '', data = {}, timeout_time = 10000, config) {
  // merge default config & any passed in config
  this.config = {
    ...defaultConfig,
    ...config
  }
  // Default options are marked with *
  return timeout(timeout_time, fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: this.config.mode,
    cache: this.config.cache,
    credentials: this.config.credentials,
    headers: {
      Authorization: store.state.headerAuth,
      'Content-Type': 'application/json'
    },
    redirect: this.config.redirect,
    referrerPolicy: this.config.referrerPolicy,
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  }))
    .then(blob => {
      if (blob.ok) {
        return blob.text()
      } else {
        const error = 'fetch not successful'
        return Promise.reject(error)
      }
    })
    .then(text => {
      try {
        return JSON.parse(text)
      } catch {
        return Promise.reject(text)
      }
    })
    .catch(error => Promise.reject(error))
}

function getRequest (url = '', config = {}) {
  // merge default config & any passed in config
  this.config = {
    ...defaultConfig,
    ...config
  }
  // Default options are marked with *
  return timeout(10000, fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    mode: this.config.mode,
    cache: this.config.cache,
    credentials: this.config.credentials,
    headers: {
      Authorization: store.state.headerAuth,
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: this.config.redirect,
    referrerPolicy: this.config.referrerPolicy
  }))
    .then(blob => {
      if (blob.ok) {
        return blob.text()
      } else {
        const error = 'fetch not successful'
        return Promise.reject(error)
      }
    })
    .then(text => {
      try {
        return JSON.parse(text)
      } catch {
        return Promise.reject(text)
      }
    })
    .catch(error => Promise.reject(error))
}

function timeout (ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(new Error('timeout'))
    }, ms)
    promise.then(resolve, reject)
  })
}
