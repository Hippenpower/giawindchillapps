import mockedUsermanagement from './response/config/usermanagement/usermanagement.json'
import mockedSynchronizer from './response/config/synchronizer/synchronizer.json'

export function getConfig (body, resolve, reject) {
  switch (true) {
    case body.app === 'usermanagement':
      if (body.config === 'active') {
        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(mockedUsermanagement)) })
      }
      break
    case body.app === 'synchronizer':
      if (body.config === 'active') {
        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(mockedSynchronizer)) })
      }
      break
    default:
      resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message: 'config for app not defined' })) })
  }
}
