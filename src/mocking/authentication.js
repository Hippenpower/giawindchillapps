import mockedAuth from './response/userauthresponse.json'

export function handleAuthentication (body, resolve, reject) {
  if (body.password === 'test') {
    mockedAuth.username = body.user

    resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(mockedAuth)) })
  } else if (body.username === 'connection') {
    const error = { ok: false, code: 500 }
    reject(error)
  } else {
    const errorResponse = { code: 401, messages: 'Username or password is incorrect' }
    const error = { ok: true, text: () => Promise.resolve(JSON.stringify(errorResponse)) }
    resolve(error)
  }
}
