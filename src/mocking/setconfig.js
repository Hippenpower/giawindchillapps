
export function setConfig (body, resolve, reject) {
  switch (true) {
    case body.app === 'usermanagement':
      resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({ message: 'config successfully saved' })) })
      break
    case body.app === 'synchronizer':
      resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({ message: 'config successfully saved' })) })
      break
    default:
      resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message: 'config for app not defined' })) })
  }
}
