import { handleAuthentication } from './authentication'
import { getInstalledApps } from './getinstalledapps'
import { getConfig } from './getconfig'
import { setConfig } from './setconfig'

export default function () {
  const realFetch = window.fetch
  window.fetch = function (url, opts) {
    return new Promise((resolve, reject) => {
      // wrap in timeout to simulate server api call
      setTimeout(() => {
        handleRoute(url, opts, realFetch, resolve, reject)
      }, 500)
    })
  }
}

function handleRoute (url, opts, realFetch, resolve, reject) {
  const { method } = opts
  const body = opts.body && JSON.parse(opts.body)
  switch (true) {
    case url.endsWith('authentification.jsp') && method === 'POST':
      return handleAuthentication(body, resolve, reject)
    case url.endsWith('getinstalledapps.jsp') && method === 'POST':
      return getInstalledApps(resolve)
    case url.endsWith('getconfig.jsp') && method === 'POST':
      return getConfig(body, resolve, reject)
    case url.endsWith('setconfig.jsp') && method === 'POST':
      return setConfig(body, resolve, reject)
    default:
      return realFetch(url, opts)
        .then(response => resolve(response))
        .catch(error => reject(error))
  }
}
