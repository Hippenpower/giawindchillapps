import mockedInstalledApps from './response/installedApps.json'

export function getInstalledApps (resolve) {
  resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(mockedInstalledApps)) })
}
