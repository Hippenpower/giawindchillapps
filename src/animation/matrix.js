const defaultConfig = {
  textStrip: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
  stripCount: 90,
  stripX: [],
  stripY: [],
  dY: [],
  stripFontSize: [],
  theColors: ['#cefbe4', '#81ec72', '#5cd646', '#54d13c', '#4ccc32', '#43c728'],
  interval: 0
}

export class Matrix {
  constructor (canvasID = 'theMatrix', config) {
    this.canvas = document.getElementById(canvasID)
    this.ctx = this.canvas.getContext('2d')
    this.canW = parseInt(this.canvas.getAttribute('width'))
    this.canH = parseInt(this.canvas.getAttribute('height'))

    // merge default config & any passed in config
    this.config = {
      ...defaultConfig,
      ...config
    }
    this.addListerners()
    this.initCanvas()
    this.initialSetup()
    window.requestAnimationFrame(() => this.throttleFPS())
  }

  addListerners () {
    // const { config } = this
    // Resize listener
    window.addEventListener('resize', () => {
      this.initialSetup()
    })
  }

  initCanvas () {
    const { canvas } = this
    canvas.setAttribute('width', window.innerWidth)
    canvas.setAttribute('height', window.innerHeight)

    this.canW = parseInt(canvas.getAttribute('width'))
    this.canH = parseInt(canvas.getAttribute('height'))
  }

  initialSetup () {
    const { config } = this
    for (var i = 0; i < config.stripCount; i++) {
      config.stripX[i] = Math.floor(Math.random() * 1920)
      config.stripY[i] = -100
      config.dY[i] = Math.floor(Math.random() * 7) + 3
      config.stripFontSize[i] = Math.floor(Math.random() * 24) + 12
    }
  }

  drawStrip (x, y) {
    const { ctx, config } = this
    for (var k = 0; k <= 20; k++) {
      var randChar = config.textStrip[Math.floor(Math.random() * config.textStrip.length)]
      if (ctx.fillText) {
        switch (k) {
          case 0:
            ctx.fillStyle = config.theColors[0]; break
          case 1:
            ctx.fillStyle = config.theColors[1]; break
          case 3:
            ctx.fillStyle = config.theColors[2]; break
          case 7:
            ctx.fillStyle = config.theColors[3]; break
          case 13:
            ctx.fillStyle = config.theColors[4]; break
          case 17:
            ctx.fillStyle = config.theColors[5]; break
        }
        ctx.fillText(randChar, x, y)
      }
      y -= config.stripFontSize[k]
    }
  }

  draw () {
    const { canvas, ctx, config } = this
    // clear the canvas and set the properties
    ctx.shadowOffsetX = ctx.shadowOffsetY = 0
    ctx.shadowBlur = 5
    ctx.shadowColor = '#94f475'

    for (var j = 0; j < config.stripCount; j++) {
      ctx.font = config.stripFontSize[j] + 'px MatrixCode'
      // console.log(context.font)
      ctx.textBaseline = 'top'
      ctx.textAlign = 'center'

      if (config.stripY[j] > 1358) {
        config.stripX[j] = Math.floor(Math.random() * canvas.width)
        config.stripY[j] = -100
        config.dY[j] = Math.floor(Math.random() * 7) + 3
        config.stripFontSize[j] = Math.floor(Math.random() * 24) + 12
        this.drawStrip(config.stripX[j], config.stripY[j])
      } else this.drawStrip(config.stripX[j], config.stripY[j])

      config.stripY[j] += config.dY[j]
    }
  }

  // Render
  render () {
    // console.log('is rendering')
    const { ctx } = this
    ctx.clearRect(0, 0, this.canW, this.canH)
    this.draw()
    this.throttleFPS()
  }

  throttleFPS () {
    // const { config } = this
    this.interval = setTimeout(
      () => {
        window.requestAnimationFrame(() => this.render())
      }, 1000 / 30
    )
  }
}

export default Matrix
