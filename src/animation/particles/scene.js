const defaultConfig = {
  ballColor: {
    r: 0,
    g: 0,
    b: 0
  },
  R: 3,
  balls: [],
  alphaF: 0.03,
  // var alpha_phase = 0

  // Line
  linkLineWidth: 1,
  disLimit: 150,
  // var add_mouse_point = true
  // var mouseIn = false
  mouseBall: {
    x: 0,
    y: 0,
    vx: 0,
    vy: 0,
    r: 0,
    type: 'mouse'
  },
  numOfBalls: 150
}

export class Scene {
  constructor (canvasID = 'nokey', config) {
    this.canvas = document.getElementById(canvasID)
    this.ctx = this.canvas.getContext('2d')
    this.canW = parseInt(this.canvas.getAttribute('width'))
    this.canH = parseInt(this.canvas.getAttribute('height'))

    // merge default config & any passed in config
    this.config = {
      ...defaultConfig,
      ...config
    }
    this.addListerners()
    this.config.numOfBalls = this.initCanvas()
    this.initBalls(this.config.numOfBalls)
    // document.body.style.background = 'url(' + this.canvas.toDataURL() + ')'
  }

  startAnimation () {
    window.requestAnimationFrame(() => this.render())
  }

  addListerners () {
    const { config } = this
    // Mouse effect
    this.canvas.addEventListener('mouseenter', function () {
      // mouseIn = true
      config.balls.push(config.mouseBall)
    })
    this.canvas.addEventListener('mouseleave', function () {
      // mouseIn = false
      var newBalls = []
      Array.prototype.forEach.call(config.balls, function (b) {
        if (!Object.prototype.hasOwnProperty.call(b, 'type')) {
          newBalls.push(b)
        }
      })
      config.balls = newBalls.slice(0)
    })
    this.canvas.addEventListener('mousemove', function (e) {
      e = window.event
      config.mouseBall.x = e.pageX
      config.mouseBall.y = e.pageY
      // console.log(mouseBall);
    })

    window.addEventListener('resize', () => {
      config.numOfBalls = this.initCanvas()
      this.initBalls(config.numOfBalls)
    })
  }

  // Init Canvas
  initCanvas () {
    const { canvas } = this
    canvas.setAttribute('width', window.innerWidth)
    canvas.setAttribute('height', window.innerHeight)

    this.canW = parseInt(canvas.getAttribute('width'))
    this.canH = parseInt(canvas.getAttribute('height'))

    return (this.canH * this.canW / 7000)
  }

  // Init Balls
  initBalls (num) {
    this.config.balls = []
    for (var i = 1; i <= num; i++) {
      this.config.balls.push({
        x: this.randomSidePos(this.canW),
        y: this.randomSidePos(this.canH),
        vx: this.getRandomSpeed('top')[0],
        vy: this.getRandomSpeed('top')[1],
        r: this.config.R,
        alpha: 1,
        phase: this.randomNumFrom(0, 10)
      })
    }
  }

  // Random speed
  getRandomSpeed (pos) {
    var min = -1
    var max = 1
    switch (pos) {
      case 'top':
        return [this.randomNumFrom(min, max), this.randomNumFrom(0.1, max)]
      case 'right':
        return [this.randomNumFrom(min, -0.1), this.randomNumFrom(min, max)]
      case 'bottom':
        return [this.randomNumFrom(min, max), this.randomNumFrom(min, -0.1)]
      case 'left':
        return [this.randomNumFrom(0.1, max), this.randomNumFrom(min, max)]
      default:

        break
    }
  }

  randomArrayItem (arr) {
    return arr[Math.floor(Math.random() * arr.length)]
  }

  randomNumFrom (min, max) {
    return Math.random() * (max - min) + min
  }

  randomSidePos (length) {
    return Math.ceil(Math.random() * length)
  }

  updateBackground () {
    // console.log('update background')
    const { canvas } = this
    // this.img = document.getElementById('app')
    document.body.style.background = 'url(' + canvas.toDataURL() + ')'
    document.body.style.zIndex = 10
  }

  throttleFPS () {
    // console.log(this)
    setTimeout(
      () => {
        window.requestAnimationFrame(() => this.render())
      }, 1000 / 20
    )
  }

  // Render
  render () {
    const { ctx } = this
    ctx.clearRect(0, 0, this.canW, this.canH)
    this.renderBalls()
    this.renderLines()
    this.updateBalls()
    this.addBallIfy()
    // this.updateBackground()
    this.throttleFPS()
  }

  // Draw Ball
  renderBalls () {
    const { ctx, config } = this
    Array.prototype.forEach.call(this.config.balls, function (b) {
      if (!Object.prototype.hasOwnProperty.call(b, 'type')) {
        ctx.fillStyle = 'rgba(' + config.ballColor.r + ',' + config.ballColor.g + ',' + config.ballColor.b + ',' + b.alpha + ')'
        ctx.beginPath()
        ctx.arc(b.x, b.y, config.R, 0, Math.PI * 2, true)
        ctx.closePath()
        ctx.fill()
      }
    })
  }

  // Update balls
  updateBalls () {
    // console.log(this)
    const { config, canW, canH } = this
    var newBalls = []
    Array.prototype.forEach.call(config.balls, function (b) {
      b.x += b.vx
      b.y += b.vy

      if (b.x > -(50) && b.x < (canW + 50) && b.y > -(50) && b.y < (canH + 50)) {
        newBalls.push(b)
      }

      // alpha change
      b.phase += config.alphaF
      b.alpha = Math.abs(Math.cos(b.phase))
      // console.log(b.alpha);
    })

    config.balls = newBalls.slice(0)
  }

  // Draw lines
  renderLines () {
    const { ctx, config } = this
    var fraction, alpha
    for (var i = 0; i < config.balls.length; i++) {
      for (var j = i + 1; j < config.balls.length; j++) {
        fraction = this.getDisOf(config.balls[i], config.balls[j]) / config.disLimit

        if (fraction < 1) {
          alpha = (1 - fraction).toString()

          ctx.strokeStyle = 'rgba(150,150,150,' + alpha + ')'
          ctx.lineWidth = config.linkLineWidth

          ctx.beginPath()
          ctx.moveTo(config.balls[i].x, config.balls[i].y)
          ctx.lineTo(config.balls[j].x, config.balls[j].y)
          ctx.stroke()
          ctx.closePath()
        }
      }
    }
  }

  // calculate distance between two points
  getDisOf (b1, b2) {
    var deltaX = Math.abs(b1.x - b2.x)
    var deltaY = Math.abs(b1.y - b2.y)

    return Math.sqrt(deltaX * deltaX + deltaY * deltaY)
  }

  // add balls if there a little balls
  addBallIfy () {
    if (this.config.balls.length < this.config.numOfBalls) {
      this.config.balls.push(this.getRandomBall())
    }
  }

  getRandomBall () {
    var pos = this.randomArrayItem(['top', 'right', 'bottom', 'left'])
    switch (pos) {
      case 'top':
        return {
          x: this.randomSidePos(this.canW),
          y: -this.config.R,
          vx: this.getRandomSpeed('top')[0],
          vy: this.getRandomSpeed('top')[1],
          r: this.config.R,
          alpha: 1,
          phase: this.randomNumFrom(0, 10)
        }
      case 'right':
        return {
          x: this.canW + this.config.R,
          y: this.randomSidePos(this.canH),
          vx: this.getRandomSpeed('right')[0],
          vy: this.getRandomSpeed('right')[1],
          r: this.config.R,
          alpha: 1,
          phase: this.randomNumFrom(0, 10)
        }
      case 'bottom':
        return {
          x: this.randomSidePos(this.canW),
          y: this.canH + this.config.R,
          vx: this.getRandomSpeed('bottom')[0],
          vy: this.getRandomSpeed('bottom')[1],
          r: this.config.R,
          alpha: 1,
          phase: this.randomNumFrom(0, 10)
        }
      case 'left':
        return {
          x: -this.config.R,
          y: this.randomSidePos(this.canH),
          vx: this.getRandomSpeed('left')[0],
          vy: this.getRandomSpeed('left')[1],
          r: this.config.R,
          alpha: 1,
          phase: this.randomNumFrom(0, 10)
        }
    }
  }
}

export default Scene
