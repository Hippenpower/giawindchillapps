export const particles = {
  goMovie
}

var canvas = document.getElementById('nokey')
var canW = parseInt(canvas.getAttribute('width'))
var canH = parseInt(canvas.getAttribute('height'))
var ctx = canvas.getContext('2d')

// console.log(typeof canW);
/*
var ball = {
  x: 0,
  y: 0,
  vx: 0,
  vy: 0,
  r: 0,
  alpha: 1,
  phase: 0
}
*/

var ballColor = {
  r: 207,
  g: 255,
  b: 4
}
var R = 2
var balls = []
var alphaF = 0.03
// var alpha_phase = 0

// Line
var linkLineWidth = 0.8
var disLimit = 260
// var add_mouse_point = true
// var mouseIn = false
var mouseBall = {
  x: 0,
  y: 0,
  vx: 0,
  vy: 0,
  r: 0,
  type: 'mouse'
}

// Random speed
function getRandomSpeed (pos) {
  var min = -1
  var max = 1
  switch (pos) {
    case 'top':
      return [randomNumFrom(min, max), randomNumFrom(0.1, max)]
    case 'right':
      return [randomNumFrom(min, -0.1), randomNumFrom(min, max)]
    case 'bottom':
      return [randomNumFrom(min, max), randomNumFrom(min, -0.1)]
    case 'left':
      return [randomNumFrom(0.1, max), randomNumFrom(min, max)]
    default:

      break
  }
}
function randomArrayItem (arr) {
  return arr[Math.floor(Math.random() * arr.length)]
}
function randomNumFrom (min, max) {
  return Math.random() * (max - min) + min
}
// Random Ball
function getRandomBall () {
  var pos = randomArrayItem(['top', 'right', 'bottom', 'left'])
  switch (pos) {
    case 'top':
      return {
        x: randomSidePos(canW),
        y: -R,
        vx: getRandomSpeed('top')[0],
        vy: getRandomSpeed('top')[1],
        r: R,
        alpha: 1,
        phase: randomNumFrom(0, 10)
      }
    case 'right':
      return {
        x: canW + R,
        y: randomSidePos(canH),
        vx: getRandomSpeed('right')[0],
        vy: getRandomSpeed('right')[1],
        r: R,
        alpha: 1,
        phase: randomNumFrom(0, 10)
      }
    case 'bottom':
      return {
        x: randomSidePos(canW),
        y: canH + R,
        vx: getRandomSpeed('bottom')[0],
        vy: getRandomSpeed('bottom')[1],
        r: R,
        alpha: 1,
        phase: randomNumFrom(0, 10)
      }
    case 'left':
      return {
        x: -R,
        y: randomSidePos(canH),
        vx: getRandomSpeed('left')[0],
        vy: getRandomSpeed('left')[1],
        r: R,
        alpha: 1,
        phase: randomNumFrom(0, 10)
      }
  }
}
function randomSidePos (length) {
  return Math.ceil(Math.random() * length)
}

// Draw Ball
function renderBalls () {
  Array.prototype.forEach.call(balls, function (b) {
    if (!Object.prototype.hasOwnProperty.call(b, 'type')) {
      ctx.fillStyle = 'rgba(' + ballColor.r + ',' + ballColor.g + ',' + ballColor.b + ',' + b.alpha + ')'
      ctx.beginPath()
      ctx.arc(b.x, b.y, R, 0, Math.PI * 2, true)
      ctx.closePath()
      ctx.fill()
    }
  })
}

// Update balls
function updateBalls () {
  var newBalls = []
  Array.prototype.forEach.call(balls, function (b) {
    b.x += b.vx
    b.y += b.vy

    if (b.x > -(50) && b.x < (canW + 50) && b.y > -(50) && b.y < (canH + 50)) {
      newBalls.push(b)
    }

    // alpha change
    b.phase += alphaF
    b.alpha = Math.abs(Math.cos(b.phase))
    // console.log(b.alpha);
  })

  balls = newBalls.slice(0)
}

// loop alpha
/*
function loopAlphaInf () {

}
*/

// Draw lines
function renderLines () {
  var fraction, alpha
  for (var i = 0; i < balls.length; i++) {
    for (var j = i + 1; j < balls.length; j++) {
      fraction = getDisOf(balls[i], balls[j]) / disLimit

      if (fraction < 1) {
        alpha = (1 - fraction).toString()

        ctx.strokeStyle = 'rgba(150,150,150,' + alpha + ')'
        ctx.lineWidth = linkLineWidth

        ctx.beginPath()
        ctx.moveTo(balls[i].x, balls[i].y)
        ctx.lineTo(balls[j].x, balls[j].y)
        ctx.stroke()
        ctx.closePath()
      }
    }
  }
}

// calculate distance between two points
function getDisOf (b1, b2) {
  var deltaX = Math.abs(b1.x - b2.x)
  var deltaY = Math.abs(b1.y - b2.y)

  return Math.sqrt(deltaX * deltaX + deltaY * deltaY)
}

// add balls if there a little balls
function addBallIfy () {
  if (this.config.balls.length < 80) {
    this.config.balls.push(getRandomBall())
  }
}

// Render
function render () {
  ctx.clearRect(0, 0, canW, canH)
  renderBalls()
  renderLines()
  updateBalls()
  addBallIfy()
  window.requestAnimationFrame(render)
}

// Init Balls
function initBalls (num) {
  for (var i = 1; i <= num; i++) {
    balls.push({
      x: randomSidePos(canW),
      y: randomSidePos(canH),
      vx: getRandomSpeed('top')[0],
      vy: getRandomSpeed('top')[1],
      r: R,
      alpha: 1,
      phase: randomNumFrom(0, 10)
    })
  }
}
// Init Canvas
function initCanvas () {
  canvas.setAttribute('width', window.innerWidth)
  canvas.setAttribute('height', window.innerHeight)

  canW = parseInt(canvas.getAttribute('width'))
  canH = parseInt(canvas.getAttribute('height'))
}
window.addEventListener('resize', function (e) {
  initCanvas()
})

function goMovie () {
  initCanvas()
  initBalls(30)
  window.requestAnimationFrame(render)
}

// Mouse effect
canvas.addEventListener('mouseenter', function () {
  // mouseIn = true
  balls.push(mouseBall)
})
canvas.addEventListener('mouseleave', function () {
  // mouseIn = false
  var newBalls = []
  Array.prototype.forEach.call(balls, function (b) {
    if (!Object.prototype.hasOwnProperty.call(b, 'type')) {
      newBalls.push(b)
    }
  })
  balls = newBalls.slice(0)
})
canvas.addEventListener('mousemove', function (e) {
  e = window.event
  mouseBall.x = e.pageX
  mouseBall.y = e.pageY
  // console.log(mouseBall);
})
