import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import i18n from './lang/index'
// import Adal from 'vue-adal'

import 'buefy/dist/buefy.css'
import '@mdi/font/css/materialdesignicons.css'
import fullscreen from 'vue-fullscreen'

// import 'vue-material/dist/vue-material.min.css'

import api from './mocking/api'
import authentication from './services/ssoauthentication'

Vue.use(Buefy)
Vue.use(fullscreen)
Vue.config.productionTip = false

// remove error message that should be info (warning message)
const ignoreWarnMessage = 'The .native modifier for v-on is only valid on components but it was used on <a>.'
Vue.config.warnHandler = function (msg, vm, trace) {
  // `trace` is the component hierarchy trace
  if (msg === ignoreWarnMessage) {
    msg = null
    vm = null
    trace = null
  }
}

if (process.env.VUE_APP_MOCK_API.toLowerCase() === 'true') {
  api()
}

if (process.env.VUE_APP_USE_SSO.toLowerCase() === 'true') {
  authentication.initialize().then(_ => {
    new Vue({
      router,
      store,
      i18n,
      render: h => h(App)
    }).$mount('#app')
  })
} else {
  new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
  }).$mount('#app')
}
