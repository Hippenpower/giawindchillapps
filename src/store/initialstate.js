import allApps from '../json_files/gia_apps_products'
const defaultState = () => {
  return {
    user: {
      userName: ''
    },
    loggedIn: false,
    headerAuth: '',
    userName: '',
    apps: allApps,
    md5: -1,
    performingLoggin: false,
    NumberOfProcessingElements: 0,
    currentIndexOfElement: 0,
    settings: {
      user: {
        language: '',
        timestamp: 0,
        mode: 'auto',
        darkmode: false,
        notification: {
          usermanagement: false,
          neutraldata: false,
          publishqueuemanager: false,
          publishfromlist: false,
          customattributes: false,
          sapchecker: false,
          publishmanager: false,
          synchronizer: false
        }
      },
      customattributes: {
        columnsVisible: {
          number: true,
          version: true,
          lifecycle: true,
          authoringApplication: true,
          modifiedOn: true
        },
        exactSearch: true
      },
      publishfromlist: {
        columnsVisible: {
          status: true,
          number: true,
          version: true,
          lifecycle: true,
          authoringApplication: true,
          modifiedOn: true,
          modifier: true,
          queueSet: true,
          queuePriority: true,
          viewableLink: true,
          structureType: true,
          publishRuleList: true,
          jobSource: true,
          forceRepublish: true
        }
      },
      synchronizer: {
        columnsVisible: {
          found: true,
          number: true,
          version: true,
          lifecycle: true,
          modifiedOn: true,
          modifier: true
        }
      }
    }
  }
}

export default defaultState
