const getters = {
  isLegit: (state) => (appName) => {
    var legitStates = { configview: false, config: false, app: false, appview: false, nopermission: true }

    if (!state.loggedIn) {
      return legitStates
    }
    if (!(appName in state.apps)) {
      return legitStates
    }
    var userLegitStates = state.apps[appName].permission

    if (userLegitStates.includes('adminpermission')) {
      for (var key in legitStates) {
        legitStates[key] = true
      }
      legitStates.nopermission = false
    }
    if (userLegitStates.includes('configfullpermission')) {
      legitStates.nopermission = false
      legitStates.config = true
      legitStates.configview = true
    }
    if (userLegitStates.includes('configreadonly')) {
      legitStates.nopermission = false
      legitStates.configview = true
    }
    if (userLegitStates.includes('functionfullpermission')) {
      legitStates.nopermission = false
      legitStates.app = true
      legitStates.appview = true
    }
    if (userLegitStates.includes('functionreadonly')) {
      legitStates.nopermission = false
      legitStates.appview = true
    }
    if (userLegitStates.includes('readonly')) {
      legitStates.nopermission = false
      legitStates.appview = true
      legitStates.configview = true
    }
    return legitStates
  },
  getAppList: (state) => {
    const appNames = Object.keys(state.apps)
    var appList = []
    if (process.env.VUE_APP_USE_SSO.toLowerCase() === 'true') {
      appNames.forEach(appName => {
        if (appName !== 'usermanagement' && state.apps[appName].is_app) {
          appList.push(state.apps[appName])
        }
      })
      return appList
    } else {
      appNames.forEach(appName => {
        if (state.apps[appName].is_app) {
          appList.push(state.apps[appName])
        }
      })
      return appList
    }
  },
  getInstalledList: state => {
    return state.apps
    /*
    const appNames = Object.keys(state.apps)
    appNames.forEach(appName => {
      var appInfo = JSON.parse(JSON.stringify(state.apps[appName]))
      appInfo.field = appName
      if (!('info' in appInfo)) {
        appInfo.info = {}
        appInfo.info.version = ''
      }
      if (!appInfo.info.license_id) {
        appInfo.info.license_id = ''
      }
      appList.push(appInfo)
    })
    */
  }
}

export default getters
