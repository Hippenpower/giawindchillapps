import { apiService } from '@/api/index'
import Vue from 'vue'

const inverval = () => {
  return {
    // checkLogFileIntervall: null,
    checkLogFileIntervall: null
  }
}

const LogLevelModule = {
  namespaced: true,
  state () {
    return {
      loggerConfig: {},
      loadingConfig: false,
      logFiles: [],
      mainTab: 0,
      activeLogFile: '',
      activeTab: 0,
      selectedLogLevel: 10,
      searchString: '',
      loadingLogFile: false
    }
  },
  actions: {
    subscribeForLoggerLevels (context) {
      context.state.loadingConfig = true
      return new Promise((resolve, reject) => {
        apiService.getRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'loghandling/loglevel.jsp')
          .then(data => {
            context.commit('setLogLevels', data)
            resolve()
          })
          .finally(() => {
            context.state.loadingConfig = false
          })
      })
    },
    getAllLogFiles (context) {
      return new Promise((resolve, reject) => {
        apiService.getRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'loghandling/logstream.jsp')
          .then(data => {
            context.commit('setLogFiles', data)
            context.dispatch('subscribe2LogFiles')
            resolve(data.files[0])
          })
      })
    },
    subscribe2LogFiles (context) {
      if (inverval.checkLogFileIntervall !== null) {
        inverval.checkLogFileIntervall = setInterval(function () {
          apiService.getRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'loghandling/logstream.jsp')
            .then(data => {
              context.commit('setLogFiles', data)
            })
        }, 5000)
      }
    },
    getLogContent (context) {
      context.state.loadingLogFile = true
      if (inverval.checkLogContentInterval !== null) {
        inverval.checkLogContentInterval = setInterval(function () {
          var payload = JSON.parse(JSON.stringify(context.state.logFiles[context.state.activeTab]))
          delete payload.content
          apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'loghandling/logstream.jsp', payload)
            .then(data => {
              context.commit('setLogContent', data)
            })
            .finally(() => {
              context.state.loadingLogFile = false
            })
        }, 10000)
      }
    }
  },
  mutations: {
    setLogLevels (state, payload) {
      state.loggerConfig = payload
      Object.keys(state.loggerConfig).forEach(appName => {
        state.loggerConfig[appName].prod.forEach(logger => {
          logger.requested_level = logger.current_level
        })
      })
    },
    setLogFiles (state, payload) {
      payload.files.forEach(element => {
        var foundLogFile = state.logFiles.some((item) => {
          return item.path === element.path
        })
        if (foundLogFile) {
          state.logFiles.forEach((logFile, index) => {
            if (logFile.path === element.path) {
              if (logFile.last_modified !== element.last_modified && index !== state.activeTab) {
                logFile.tag = 'update'
              }
            }
          })
        } else {
          element.tag = 'new'
          state.logFiles.push(element)
        }
      })
    },
    setActiveTab (state, payload) {
      state.activeTab = payload
      state.logFiles[payload].tag = ''
    },
    setLogContent (state, payload) {
      state.logFiles.forEach(element => {
        if (element.path === payload.path) {
          Vue.set(element, 'content', payload.content)
          element.last_modified = payload.last_modified
        }
      })
    }
  },
  getters: {
    getLogContent: (state) => {
      if (state.logFiles.length > 0 && 'content' in state.logFiles[state.activeTab]) {
        return state.logFiles[state.activeTab].content
      } else {
        return null
      }
    }
  }
}

export default LogLevelModule
