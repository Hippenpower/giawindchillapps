const SearchCache = {
  namespaced: true,
  state: () => ({
    cache: []
  }),
  mutations: {
    pushNewSearch (state, payload) {
      if (state.cache.length > 15) {
        state.cacheslice(0, 15)
      }
      state.cache.unshift(payload.searchString)
      localStorage.setItem(payload.moduleName, JSON.stringify(state.cache))
    },
    allEntriesFromLocalStorage (state, payload) {
      state.cache = payload
    }
  },
  actions: {
    saveNewSearch (context, payload) {
      if (!context.state.cache.includes(payload.searchString)) {
        context.commit('pushNewSearch', { searchString: payload.searchString, moduleName: payload.moduleName })
      }
    }
  },
  getters: {
    cacheEntries: (state) => (namespace) => {
      if (state.cache.length === 0) {
        var localSeachStorage = localStorage.getItem(namespace)
        if (localSeachStorage) {
          state.cache = JSON.parse(localSeachStorage)
        }
      }
      return state.cache
    }
  }
}

export default SearchCache
