import { apiService } from '@/api/index'

const inverval = () => {
  return {
    userValidInterval: null
  }
}

const UserManagement = {
  namespaced: true,
  state: {
    fetchedData: [],
    history: [],
    fetchedDataModified: null,
    editableUser: null,
    updateTimestamp: 0,
    updateIsNew: false,
    hasBeenEdited: false
  },
  mutations: {
    pushNewFetchedData (state, payload) {
      state.fetchedData = payload.config.config
      state.fetchedDataModified = payload.md5
      state.history = payload.history
    },
    addNewUser (state, payload) {
      payload.id = +new Date()
      state.fetchedData.push(payload)
      state.hasBeenEdited = true
    },
    deleteUser (state, payload) {
      state.fetchedData.splice(payload.index, 1)
      state.hasBeenEdited = true
    },
    setUserToEdit (state, payload) {
      state.editableUser = payload.row
    },
    updateUser (state, payload) {
      state.fetchedData.forEach((element, index) => {
        if (element.id === payload.id) {
          state.fetchedData.splice(index, 1)
          state.fetchedData.push(payload)
        }
      })
      state.hasBeenEdited = true
    },
    notifyForUpdate (state, payload) {
      state.updateTimestamp = payload.timestamp
      state.updateIsNew = true
    },
    resetUpdate (state) {
      state.updateTimestamp = 0
      state.updateIsNew = false
      state.hasBeenEdited = false
    },
    cleanUp (state) {
      state.updateTimestamp = 0
      state.updateIsNew = false
      clearInterval(inverval.userValidInterval)
      state.hasBeenEdited = false
    }
  },
  actions: {
    getEligibleData (context) {
      return new Promise((resolve, reject) => {
        const body = { app: 'usermanagement', config: 'active' }
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'getconfig.jsp', body)
          .then(data => {
            if (data) {
              if (data.error) {
                if (data.error == null) {
                  context.commit('pushNewFetchedData', data)
                  resolve()
                }
              } else {
                context.commit('pushNewFetchedData', data)
                resolve()
              }
            }
            const error = 'Error: ' + data.error
            reject(error)
          })
      })
    },
    checkForUpdate (context) {
      inverval.userValidInterval = setInterval(function () {
        const body = { app: 'usermanagement', config: 'active' }
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'getconfig.jsp', body)
          .then(data => {
            if (data) {
              if (data.error) {
              } else {
                if (data.md5 !== context.state.md5) {
                  context.commit('pushNewFetchedData', data)
                }
              }
            }
          })
      }, 4400)
    },
    commitConfig (context, payload) {
      return new Promise((resolve, reject) => {
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'setconfig.jsp', payload)
          .then(resolve('successful committed'))
      })
    }
  },
  getters: {
  }
}

export default UserManagement
