import { apiService } from '@/api/index'
import Vue from 'vue'

const inverval = () => {
  return {
    checkConfigInterval: null,
    checkLogInterval: null
  }
}

const ConfigLogModule = {
  namespaced: true,
  state () {
    return {
      fetchedConfig: { history: [], config: {}, turned_on: false, has_onoff: false, md5: 0 },
      fetchedLogs: [],
      updateConfigTimestamp: 0,
      updateConfigIsNew: false,
      updateLogTimestamp: 0,
      isSearchingLogFiles: false,
      isSearchingLogContent: false,
      updateLogIsNew: false,
      hasConfigBeenEdited: false
    }
  },
  mutations: {
    pushNewConfigData (state, payload) {
      Vue.set(state.fetchedConfig = JSON.parse(JSON.stringify(payload)))
    },
    pushFileConfigData (state, payload) {
      state.fetchedConfig = { ...state.fetchedConfig, config: JSON.parse(JSON.stringify(payload)) }
    },
    commitConfigPayload (state, payload) {
      if (state.fetchedConfig.history.length === 0) {
        state.fetchedConfig.history = [payload.history]
      } else {
        state.fetchedConfig.history.push(payload.history)
      }
    },
    notifyForConfigUpdate (state, payload) {
      state.updateConfigTimestamp = payload.timestamp
      state.updateConfigIsNew = true
    },
    resetConfigUpdate (state) {
      state.updateTimestamp = 0
      state.updateConfigIsNew = false
      state.hasBeenEdited = false
    },
    cleanUpConfig (state) {
      state.updateTimestamp = 0
      state.updateIsNew = false
      clearInterval(inverval.checkConfigInterval)
      state.hasBeenEdited = false
    },
    pushNewLogData (state, payload) {
      state.fetchedLogs = payload
      state.successfulLogFetch = true
    },
    isSearchingLogFiles (state) {
      state.isSearchingLogFiles = true
    },
    isSearchingLogContent (state) {
      state.isSearchingLogContent = true
    },
    notifyForLogUpdate (state, payload) {
      payload.forEach(element => {
        element.is_new = true
        state.fetchedLogs.push(element)
      })
      state.updateLogIsNew = true
    },
    resetLogUpdate (state) {
      state.updateLogTimestamp = 0
      state.updateLogIsNew = false
    },
    cleanUpLog (state) {
      state.updateLogTimestamp = 0
      state.updateLogIsNew = false
      clearInterval(inverval.checkLogInterval)
    }
  },
  actions: {
    fetchActiveConfig (context, payload) {
      return new Promise((resolve, reject) => {
        const body = { app: payload, config: 'active' }
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'getconfig.jsp', body)
          .then(data => {
            if ('error' in data) {
              const error = 'There is an error in fetch data <br> Error: ' + data.error
              reject(error)
            } else {
              context.commit('pushNewConfigData', data)
              resolve(data)
            }
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    subscribeForConfigUpdate (context, payload) {
      inverval.checkConfigInterval = setInterval(function () {
        const body = { app: payload, config: 'active', update: true }
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'getconfig.jsp', body)
          .then(data => {
            if (data.md5 === context.state.md5) {
            } else {
              context.commit('notifyForConfigUpdate', { md5: data.md5 })
            }
          })
      }, 4400)
    },
    commitConfig (context, payload) {
      return new Promise((resolve, reject) => {
        apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'setconfig.jsp', payload)
          .then(data => {
            context.commit('commitConfigPayload', payload)
          })
          .then(resolve('successful committed'))
      })
    }
  },
  getters: {
  }
}

export default ConfigLogModule
