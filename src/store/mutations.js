import defaultState from './initialstate'
import Vue from 'vue'

const mutations = {
  setLogin (state, payload) {
    if (process.env.VUE_APP_USE_SSO.toLowerCase() === 'true') {
      state.user = payload
      const allApps = Object.keys(state.apps)
      allApps.forEach(element => {
        state.apps[element].permission = 'adminpermission'
      })
      state.loggedIn = true
    } else {
      state.headerAuth = payload.header_auth
      state.loggedIn = payload.successful
      state.user.userName = payload.username
      const allApps = Object.keys(state.apps)
      allApps.forEach(element => {
        if (element in payload.applist) {
          state.apps[element].permission = payload.applist[element]
        }
      })
      state.md5 = payload.md5
    }
  },
  setLogout (state) {
    Object.assign(state, defaultState())
  },
  updatePermission (state, payload) {
    state.apps = payload.applist
    state.md5 = payload.md5
  },
  setUserSettings (state, payload) {
    if ('user' in payload) {
      if ('language' in payload.user) {
        state.settings.user.language = payload.user.language
      }
      if ('darkmode' in payload.user) {
        state.settings.user.darkmode = payload.user.darkmode
      }
      if ('mode' in payload.user) {
        state.settings.user.mode = payload.user.mode
      }
      if ('notification' in payload.user) {
        state.settings.user.notification = payload.user.notification
      }
    }
    if ('customattributes' in payload) {
      const allHeader = Object.keys(payload.customattributes)
      allHeader.forEach(element => {
        state.settings.customattributes[element] = payload.customattributes[element]
      })
    }
    if ('publishfromlist' in payload) {
      const allHeader = Object.keys(payload.publishfromlist)
      allHeader.forEach(element => {
        state.settings.publishfromlist[element] = payload.publishfromlist[element]
      })
    }
    localStorage.setItem('settings', JSON.stringify(state.settings))
  },
  setInstalledApps  (state, payload) {
    const allApps = Object.keys(payload)
    allApps.forEach(element => {
      if (element in state.apps) {
        state.apps[element].isinstalled = true
        state.apps[element].info = payload[element]
      } else {
        if (JSON.stringify(payload[element]) !== '{}') {
          state.apps[element] = {}
          state.apps[element].info = payload[element]
        }
      }
    })
  },
  performingLoggin (state, payload) {
    state.performingLoggin = payload
  },
  updateProgress (state) {
    state.currentIndexOfElement++
    if (state.currentIndexOfElement > state.NumberOfProcessingElements) {
      state.currentIndexOfElement = 0
    }
  },
  initializeProgress (state, payload) {
    state.currentIndexOfElement = 0
    state.NumberOfProcessingElements = payload
  },
  newLicenseDates (state, payload) {
    var appNames = Object.keys(state.apps)
    appNames.forEach(appName => {
      var app = state.apps[appName]
      if ('info' in app) {
        if (app.info.identifier === payload.id) {
          Vue.set(state.apps[appName].info, 'valid_until', payload.c)
          Vue.set(state.apps[appName].info, 'license_id', payload.license_id)
        }
      }
    })
  }
}
export default mutations
