import { userService } from '../services'
import i18n from '../lang'
import { apiService } from '../api/index'

var userValidInterval = null

const actions = {
  loggingProzess (context, payload) {
    context.commit('setLogin', payload)
    // context.dispatch('checkingCredentials')
  },
  logoutProzess (context) {
    context.commit('setLogout')
    clearInterval(userValidInterval)
  },
  checkingCredentials (context) {
    userValidInterval = setInterval(function () {
      userService.update()
        .then(response => {
          if (response.md5 !== context.state.md5) {
            userService.updatePermissions()
          }
        })
    }, 60000)
  },
  setUserSettings (context, payload) {
    const settings = localStorage.getItem('settings')
    if (!settings || payload.force) {
      if (payload.force) {
        if ('user' in payload) {
          if (payload.user.mode === 'auto') {
            payload.user.darkmode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
          } else if (payload.user.mode === 'normal') {
            payload.user.darkmode = false
          } else if (payload.user.mode === 'dark') {
            payload.user.darkmode = true
          }
        }
        context.commit('setUserSettings', payload)
      } else {
        payload.user.darkmode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
        payload.user.mode = 'auto'
        context.commit('setUserSettings', payload)
      }
    } else {
      var localSettings = JSON.parse(settings)
      if (localSettings.user.mode === 'auto') {
        localSettings.user.darkmode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
      } else if (localSettings.user.mode === 'normal') {
        localSettings.user.darkmode = false
      } else if (localSettings.user.mode === 'dark') {
        localSettings.user.darkmode = true
      }
      context.commit('setUserSettings', localSettings)
    }
    context.dispatch('changeStyling', payload)
    context.dispatch('changeLanguage')
  },
  changeStyling ({ commit, state }, payload) {
    var style = document.getElementById('darkmode')
    var styleJsonEditor = document.getElementById('jsoneditordarkmode')
    if (style) {
      if (state.settings.user.darkmode) {
        style.disabled = false
        styleJsonEditor.disabled = false
      } else {
        style.disabled = true
        styleJsonEditor.disabled = true
      }
    } else {
      if (state.settings.user.darkmode) {
        style = document.createElement('link')
        style.type = 'text/css'
        style.rel = 'stylesheet'
        style.href = process.env.VUE_APP_BASE_URL + process.env.VUE_APP_FRONTEND_EXTENSION + 'darkmode.css'
        style.id = 'darkmode'
        document.head.appendChild(style)

        style = document.createElement('link')
        style.type = 'text/css'
        style.rel = 'stylesheet'
        style.href = process.env.VUE_APP_BASE_URL + process.env.VUE_APP_FRONTEND_EXTENSION + 'jsoneditor_darkmode.css'
        style.id = 'jsoneditordarkmode'
        document.head.appendChild(style)
      }
    }
  },
  changeLanguage (context) {
    const lang = context.state.settings.user.language
    if (i18n.locale !== lang) {
      import('../lang/locales/' + lang + '.json')
        .then((msg) => {
          i18n.locale = lang
          i18n.setLocaleMessage(lang, msg.default || msg)
        })
    }
  },
  getInstalledApps (context) {
    apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'getinstalledapps.jsp')
      .then(data => {
        context.commit('setInstalledApps', data)
      })
      .catch(error => console.log(error))
  },
  setPerfomingLogging (context, payload) {
    context.commit('performingLoggin', payload)
  },
  updateProgress (context) {
    context.commit('updateProgress')
  },
  initializeProgress (context, payload) {
    context.commit('initializeProgress', payload)
  },
  renewLicenses (context, payload) {
    context.commit('newLicenseDates', payload)
  }
}

export default actions
