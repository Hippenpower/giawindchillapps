import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import defaultState from './initialstate'
import UserManagement from './modules/usermanagementstore'
import ConfigLogModule from './modules/configlog'
import SearchCache from './modules/searchcache'
import LogLevelModule from './modules/logstatus'

Vue.use(Vuex)

export default new Vuex.Store({
  state: defaultState(),
  mutations: mutations,
  getters: getters,
  actions: actions,
  modules: {
    usermanagement: UserManagement,
    neutraldata: ConfigLogModule,
    sapchecker: ConfigLogModule,
    publishfromlist: ConfigLogModule,
    publishqueuemanager: ConfigLogModule,
    customattributes: ConfigLogModule,
    publishmonitor: ConfigLogModule,
    synchronizer: ConfigLogModule,
    synchronizercache: SearchCache,
    additionalattributescache: SearchCache,
    loglevel: LogLevelModule
  }
})
