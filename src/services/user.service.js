import store from '../store'
import { apiService } from '../api/index'
import i18n from '../lang/index'

export const userService = {
  login,
  logout,
  update,
  updatePermissions
}

/**
* login function
* @param {String} username
* @param {String} password
* @param {Boolean} remember
*/

function login (username = null, password = null, rembember = false) {
  var cred = { user: username, password: password }
  return new Promise((resolve, reject) => timeout(1500, apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'authentification.jsp', cred))
    .then(response => {
      console.log(response)
      if (response.code === 200) {
        return response
      } else {
        var error = ''
        if (process.env.VUE_APP_MOCK_API.toLowerCase() === 'true') {
          error = i18n.t('login.not_authorized_message_mock')
        } else {
          error = i18n.t('login.not_authorized_message')
        }
        reject(error)
      }
    })
    .then(user => {
      // login successful if there's a user in the response
      if (user) {
        // store user details and basic auth credentials in local storage
        // to keep user logged in between page refreshes
        user.cred = 'Basic ' + window.btoa(cred.user + ':' + cred.password)
        // var enrypted = CryptoJS.AES.encrypt(JSON.stringify(user), 'Secret Passphrase')
        localStorage.setItem('user', JSON.stringify({ cred: user.cred, remember: rembember }))
        // console.log(process.env.VUE_APP_BASE_URL)
        var commitPayload = { successful: true, header_auth: user.cred, username: username, applist: user.app_list, md5: user.md5 }
        store.dispatch('loggingProzess', commitPayload)
      }
      resolve(user)
    })
    .catch(error => reject(error))
  )
}

function update () {
  // var auth = store.state.headerAuth
  // const apiBAse = 'http://gial629-va.giaintra.net:8080'
  const update = { update: true }
  return timeout(5000, apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'authentification.jsp', update))
    .then(response => {
      return response
    })
    .then(user => {
      var commitPayload = {}
      // login successful if there's a user in the response
      if (user) {
        commitPayload = { md5: user.md5 }
      }
      return commitPayload
    })
}

function updatePermissions () {
  var cred = { user: store.state.userName }
  return timeout(5000, apiService.postRequest(process.env.VUE_APP_BASE_URL + process.env.VUE_APP_API_URL + 'authentification.jsp', cred))
    .then(response => {
      if (response.code === 501) {
        return response
      } else {
        const error = 'not autorized'
        return Promise.reject(error)
      }
    })
    .then(user => {
      // login successful if there's a user in the response
      if (user) {
        // store user details and basic auth credentials in local storage
        // to keep user logged in between page refreshes
        // var enrypted = CryptoJS.AES.encrypt(JSON.stringify(user), 'Secret Passphrase')
        // console.log(process.env.VUE_APP_BASE_URL)
        var commitPayload = { applist: user.app_list, md5: user.md5 }
        store.commit('updatePermission', commitPayload)
      }
      return user
    })
}

/**
* login function
* @param {Boolean} remember
*/

function logout (remember) {
  // remove user from local storage to log user out
  store.dispatch('logoutProzess')
  if (!remember) {
    localStorage.removeItem('user')
  }
}

function timeout (ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      const error = { code: 404, messages: 'Timeout' }
      reject(error)
    }, ms)
    promise.then(resolve, reject)
  })
}
