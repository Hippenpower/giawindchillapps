// import i18n from '@/lang/index'

export const TextParser = {
  parse
}

function parse (contentString) {
  const contentSplitted = contentString.split('\n')
  const delimiter = getDelimitter(contentSplitted)
  const stringQuote = getStringQuotes(contentSplitted)

  if (contentSplitted[0].toLowerCase().indexOf(stringQuote + 'number' + stringQuote) >= 0) {
    return windchillSearchFormatEN(contentSplitted, delimiter, stringQuote)
  } else if (contentSplitted[0].toLowerCase().indexOf('"nummer"') >= 0) {
    return windchillSearchFormatDE(contentSplitted, delimiter, stringQuote)
  } else {
    return parseAsList(contentSplitted)
  }
}

function windchillSearchFormatEN (content, delimiter, stringQuote) {
  const allHeaders = content[0].toLowerCase().split(delimiter)
  const indexNumber = allHeaders.findIndex(columnName => columnName === stringQuote + 'number' + stringQuote)
  const indexVersion = allHeaders.findIndex(columnName => columnName === stringQuote + 'version' + stringQuote)

  var output = []
  if (indexNumber !== -1) {
    for (var ii = 1; ii < content.length; ii++) {
      if (content[ii].length > 1) {
        var epmInfo = {}
        var allRowContent = content[ii].split(delimiter)
        var number = allRowContent[indexNumber].slice(1, allRowContent[indexNumber].length - 1)
        epmInfo.number = number
        if (indexVersion !== -1) {
          var version = allRowContent[indexVersion].slice(1, allRowContent[indexVersion].length - 1)
          epmInfo.version = version
        }
        output.push(epmInfo)
      }
    }
  }
  return output
}

function windchillSearchFormatDE (content, delimiter, stringQuote) {
  const allHeaders = content[0].toLowerCase().split(delimiter)
  const indexNumber = allHeaders.findIndex(columnName => columnName === stringQuote + 'nummer' + stringQuote)
  const indexVersion = allHeaders.findIndex(columnName => columnName === stringQuote + 'version' + stringQuote)

  var output = []
  if (indexNumber !== -1) {
    for (var ii = 1; ii < content.length; ii++) {
      if (content[ii].length > 1) {
        var epmInfo = {}
        var allRowContent = content[ii].split(delimiter)
        var number = allRowContent[indexNumber].slice(1, allRowContent[indexNumber].length - 1)
        epmInfo.number = number
        if (indexVersion !== -1) {
          var version = allRowContent[indexVersion].slice(1, allRowContent[indexVersion].length - 1)
          epmInfo.version = version
        }
        output.push(epmInfo)
      }
    }
  }
  return output
}

function parseAsList (content) {
  var output = []
  for (var ii = 1; ii < content.length; ii++) {
    if (content[ii].length > 1) {
      var epmInfo = {}
      var allRowContent = content[ii].trim()
      epmInfo.number = allRowContent
      output.push(epmInfo)
    }
  }
  return output
}

function getDelimitter (content) {
  var delimiterOptions = [',', ';', ':']

  var delimiterCountMax = 0
  var maxCountDelimiter = ' '

  delimiterOptions.forEach(delimiterOption => {
    if (delimiterCountMax < content[0].split(delimiterOption).length - 1) {
      delimiterCountMax = content[0].split(delimiterOption).length - 1
      maxCountDelimiter = delimiterOption
    }
  })
  return maxCountDelimiter
}

function getStringQuotes (content) {
  var quoteOptions = ['"', '\'']

  var delimiterCountMax = 1
  var maxCountDelimiter = ''

  quoteOptions.forEach(quoteOption => {
    if (delimiterCountMax < content[0].split(quoteOption).length - 1) {
      delimiterCountMax = content[0].split(quoteOption).length - 1
      maxCountDelimiter = quoteOption
    }
  })
  return maxCountDelimiter
}
