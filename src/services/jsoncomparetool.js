const defaultConfig = {
  isEqualClass: 'is_equal',
  missingPropertyClass: 'is-missing',
  addedPropertyClass: 'is-added',
  differentType: 'is-diff',
  compareList: []
}

export default class JSONCompareTool {
  constructor (config = {}) {
    // merge default config & any passed in config
    this.config = {
      ...defaultConfig,
      ...config
    }
  }

  calculateDifferenence (firstObject, secondObject, path) {
    const { config } = this
    config.compareList = []
    // // console.log('start calculateDifferenence')
    // // console.log('original path: ' + path)
    const first = this.findNodeInJson(firstObject, path)
    // console.log(first)
    const second = this.findNodeInJson(secondObject, path)
    // console.log(second)
    // console.log('start deep compare')
    this.deepCompare(first, second, path)

    if (config.compareList.length > 0) {
      return 'isdifferent'
    }

    // console.log(config.compareList)
    return config.compareList
  }

  deepCompare (first, second, path) {
    // console.log('input path ' + path)
    if (!Array.isArray(path)) {
      var tempPath = path
      path = [tempPath]
    }
    const { config } = this
    var firstAllKeys = Object.keys(first)
    var secondAllKeys = Object.keys(second)
    var newPath = JSON.parse(JSON.stringify(path))
    if (firstAllKeys.length !== secondAllKeys.length) {
      config.compareList.push('unequal_keys')
      return
    }
    if (Object.prototype.toString.call(first.value) !== Object.prototype.toString.call(second.value)) {
      config.compareList.push('unequal_object')
      return
    }
    // console.log(first.value)
    if (Array.isArray(first.value)) {
      // console.log('first.value.length: ' + first.value.length)
      // console.log(second)
      if (first.value.length !== second.value.length) {
        config.compareList.push('different_array_size')
      } else {
        var firstSortedArray = first.value.sort()
        var secondSortedArray = second.value.sort()
        for (var i = 0; i < firstSortedArray.length; i++) {
          // console.log(newPath)
          // console.log('pushing: ' + i + ', to array ' + newPath)
          newPath.push(i)
          var firstArrayItem = this.getNodeInfo(firstSortedArray, i)
          var secondArrayItem = this.getNodeInfo(secondSortedArray, i)
          this.deepCompare(firstArrayItem, secondArrayItem, newPath)
        }
      }
    } else if (typeof first.value === 'string') {
      if (first.value !== second.value) {
        config.compareList.push('different_string_values')
      }
    } else if (typeof first.value === 'boolean') {
      if (first.value !== second.value) {
        config.compareList.push('different_boolean_values')
      }
    } else if (typeof first.value === 'number') {
      if (first.value !== second.value) {
        config.compareList.push('different_number_values')
      }
    } else if (Object.prototype.toString.call(first.value) === '[object Object]') {
      for (var k = 0; k < Object.keys(first.value).length; k++) {
        var key = Object.keys(first.value)[k]
        var firstObject = this.getNodeInfo(first.value, key)
        var secondObject = this.getNodeInfo(second.value, key)
        this.deepCompare(firstObject, secondObject, newPath.push(key))
      }
    }
  }

  findNodeInJson (json, path) {
    if (!json || typeof path === 'undefined' || path.length === 0) {
      return { field: undefined, value: undefined }
    }
    const first = path[0]
    const remainingPath = path.slice(1)

    if (remainingPath.length === 0) {
      return { field: (typeof json[first] !== 'undefined' ? first : undefined), value: json[first] }
    } else {
      return this.findNodeInJson(json[first], remainingPath)
    }
  }

  getNodeInfo (json, path) {
    return { field: (typeof json[path] !== 'undefined' ? path : undefined), value: json[path] }
  }
}
