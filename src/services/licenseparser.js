import CryptoJS from 'crypto-js'

export default class LicenseParser {
  constructor (file) {
    this.file = file
    this.cryptoPart = []
    this.licenseInfo = []
    this.licenseID = ''
    this.rootKey = ''

    this.subtractCrypto()
    this.subtractLicenseID()
  }

  getLicensedApplications () {
    return new Promise((resolve, reject) => {
      this.getRootKey()
        .then(() => this.getKeyList())
        .then(() => resolve(this.licenseInfo))
        .catch(() => resolve(this.licenseInfo))
    })
  }

  subtractCrypto () {
    const indexLicenseKey = this.file.indexOf('License Key')
    if (indexLicenseKey === -1) {

    }
    this.cryptoPart = this.file.substring(indexLicenseKey + 'License Key'.length + 1, this.file.length).replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ').split('hiPMat')
  }

  subtractLicenseID () {
    const indexLicenseID = this.file.indexOf('License ID:')
    const indexTimeIssued = this.file.indexOf('Time issued:')
    if (indexLicenseID === -1) {

    }
    this.licenseID = this.file.substring(indexLicenseID + 'License ID:'.length + 1, indexTimeIssued).trim()
  }

  getRootKey () {
    return new Promise((resolve, reject) => {
      fetch('http://localhost:5001/api/v1/license/getKey/' + this.licenseID, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors',
        headers: {
        // Authorization: this.config.store.state.headerAuth,
          'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
        }
      })
        .then(function (response) {
          return response.json()
        })
        .then(json => {
          this.rootKey = json.key
          resolve()
        })
        .catch(error => reject(error))
    })
  }

  getKeyList () {
    return new Promise((resolve, reject) => {
      this.cryptoPart.forEach(part => {
        var decryptedmsg = CryptoJS.AES.decrypt(part.trim(), this.rootKey)
        try {
          this.getLicenseInfo(JSON.parse(decryptedmsg.toString(CryptoJS.enc.Utf8)))
          resolve()
        } catch {
        }
      })
    })
  }

  getLicenseInfo (keyList = []) {
    keyList.forEach(item => {
      var encryptedMessage = this.cryptoPart[item.index]
      var key = item.key
      var decryptedmsg = CryptoJS.AES.decrypt(encryptedMessage, key)
      try {
        this.licenseInfo.push(JSON.parse(decryptedmsg.toString(CryptoJS.enc.Utf8)))
      } catch {
      }
    })
  }
}
