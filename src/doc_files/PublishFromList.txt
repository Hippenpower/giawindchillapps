# Windchill Publish From Text

This Java Program can be used publish Representives of a EPM Document from a text file

## Installation

Clone repository, if git install use following command in the folder you like:
```bash
git clone https://gitlab.com/Hippenpower/publishfromtxt.git
cd publishfromtxt
```

in the folder, there is a GIAPreferences_PublishTxt.xml that is used to control the output. Open Windchill shell as administrator

```bash
windchill wt.load.LoadFromFile -d "LOCATION_OF_FILE\GIAPreferences_PublishTxt.xml"
```
This will create a GIA Informatik entry with all aviable customisation.


copy\	
- "bin\ext\gia\publishText\PublishEPMObject.java"\
- "bin\ext\gia\publishText\PublishListJob.java"\
to\	 
- "\Windchill_11.0\Windchill\codebase\ext\gia\"\

Add following code to site.xconf
```xml
<Property name="schedulejobs44" overridable="true"
		targetFile="codebase/WEB-INF/conf/wvs.properties"
		value="PublishFromText"/>
<Property name="PublishFromText.description" overridable="true"
		targetFile="codebase/WEB-INF/conf/wvs.properties"
		value="GIA Publish From Text"/>
<Property name="PublishFromText.class" overridable="true"
		targetFile="codebase/WEB-INF/conf/wvs.properties"
		value="ext.gia.publishfromlist.PublishListJob"/>
<Property name="PublishFromText.method" overridable="true"
		targetFile="codebase/WEB-INF/conf/wvs.properties"
		value="doPublish"/>
<Property name="PublishFromText.jobType" overridable="true"
		targetFile="codebase/WEB-INF/conf/wvs.properties"
		value="publishJob"/>
```

Restart Windchill Servermanager
## Usage
All documentation on how to use this program should be described in the Windchill Preference Management Tab (Gia Informatik)

![Screenshot](publishfromtxt.jpg)


## Main Config Structure

The config gets processed as described below:

```mermaid
graph LR
A[Workspace] -->|Download CAD Object| B[Check Filter]
H -- exists --> C[Parameter]
B -- No Filters set --> H
C -- add Parameters --> E[Output Parameter]
B -- not passed --> F[No Outout Parameter]
H -- exists --> G[Snake Parameter]
G -- add parameter --> E
B -- Passed--> H[Check Parameter]
```

publish_txt_filepath and publish_txt_filename are optional. if not set files wil be written to D:/temp/publishfromtxt 
and the textfile is named publishlist.txt


```json
{
  "publish_txt_filepath": "D:/temp/publishfiles",
  "publish_txt_filename": "publishlist.txt",
  "config": [{
    "parameter": {},
    "filters": []
  }]
}
```


### Filter

Filter can be found at the Windchill GIA library: https://gitlab.com/Hippenpower/gia-windchill-lib

If filter is not passed, the standard values will be taken.

### Parameter
The Parameters block defines what queue name (queue_set) will be used and what priority the job has.
For the queue_priority parameter, only "H","L" and "M" are valid Strings. Default is L.
The Full parameter configuration with default values can be seen below:

```json
{
  "parameter" :{
    "queue_set": "",
    "queue_priority": "L"
  }
}
```


## Examples
if only the latest version should be published and creo and nx should be put to different queues, a config looks like that.

```json
{
  "publish_txt_filepath": "D:/temp/publishfiles",
  "publish_txt_filename": "publishlist.txt",
  "config": [
    {
      "parameter": {
        "queue_set": "proq"
      },
      "filters": [
        {
          "filter_values": [
            "UG"
          ],
          "filter_key": "authoring_application"
        },
        {
          "filter_key": "latest_version",
          "filter_boolean": false
        },
        {
          "filter_key": "latest_iteration",
          "filter_boolean": false
        }
      ]
    },
    {
      "parameter": {
        "queue_set": "ugq",
        "priority": "H"
      },
      "filters": [
        {
          "filter_values": [
            "PROE"
          ],
          "filter_key": "authoring_application"
        },
        {
          "filter_key": "latest_version",
          "filter_boolean": false
        },
        {
          "filter_key": "latest_iteration",
          "filter_boolean": true
        }
      ]
    }
  ]
}
```
## Dependencies
There are no dependencies

## Deinstallation
```bash
windchill wt.load.LoadFromFile -d "LOCATION_OF_FILE\GIAPreferences_NeutralData_delete.xml"
```

delete\
- "\ext\gia\publishText\PublishListJobLoader.java"\
- "\ext\gia\publishText\PublishListJob.java"\
and restart Windchill Server manager
## ToDos

* Should work just fine  

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## Authors

* **Matthias Hippen** - *Initial work*

## Built and Tested With

* [Windchill](https://www.ptc.com/en/products/plm/plm-products/windchill) - PTC Windchill 11.1 M030
* [Java](https://www.java.com) -  Java Version 10.0.2 2018-07-17 Java SE Runtime Enviroment 18.3

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
