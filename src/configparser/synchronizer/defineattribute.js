import Filters from '../filters'
import Rename from './rename'
import CheckAttributeFormat from './checkattributeformat'
import CutString from './cutstring'

export default DefineAttribute

function DefineAttribute (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    for (var index in keys) {
      var key = keys[index]
      if (key === 'parameter_key') {
        if (!(element[key] === 'iba_attribute' || element[key] === 'number' || element[key] === 'name')) {
          errors.push({ path: basepath.concat([key]), message: element[key] + ' is not supported, supported values: iba_attribute, number, name' })
        }
        if (element[key] === 'iba_attribute') {
          if (!('internal_attribute_name' in element && 'attribute_type' in element)) {
            errors.push({ path: basepath.concat([key]), message: element[key] + ' needs keys: internal_attribute_name, attribute_type' })
          }
        }
      } else if (key === 'internal_attribute_name') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'attribute_type') {
        if (!(element[key] === 'enumerated_list' || element[key] === 'normal')) {
          errors.push({ path: basepath.concat([key]), message: element[key] + ' is not supported, supported values: enumerated_list, normal' })
        }
      } else if (key === 'locale') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'external_reference') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'rename') {
        Rename(element[key], errors, basepath.concat([key]))
      } else if (key === 'filters') {
        Filters(element[key], errors, basepath.concat([key]))
      } else if (key === 'check_attribute_format') {
        CheckAttributeFormat(element[key], errors, basepath.concat([key]))
      } else if (key === 'cut_string') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            CutString(subElement, errors, basepath.concat([key, subIndex]))
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
        'parameter_key, internal_attribute_name, attribute_type, locale, external_reference, rename, filters, check_attribute_format, cut_string'
        })
      }
    }
  }
}
