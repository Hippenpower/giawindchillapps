export default CutString

function CutString (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    if (!keys.includes('mode')) {
      errors.push({ path: basepath.concat([]), message: 'key: mode must be set' })
    }
    for (var index in keys) {
      var key = keys[index]
      if (key === 'mode') {
        if (element[key] === 'rcut') {
          if (!keys.includes('char_length')) {
            errors.push({ path: basepath.concat([key]), message: 'this mode needs char_length to be set' })
          }
        } else if (element[key] === 'rkeep') {
          if (!keys.includes('char_length')) {
            errors.push({ path: basepath.concat([key]), message: 'this mode needs char_length to be set' })
          }
        } else if (element[key] === 'lcut') {
          if (!keys.includes('char_length')) {
            errors.push({ path: basepath.concat([key]), message: 'this mode needs char_length to be set' })
          }
        } else if (element[key] === 'lkeep') {
          if (!keys.includes('char_length')) {
            errors.push({ path: basepath.concat([key]), message: 'this mode needs char_length to be set' })
          }
        } else if (element[key] === 'char_pos') {
          if (!keys.includes('start_pos') && !keys.includes('end_pos')) {
            errors.push({ path: basepath.concat([key]), message: 'this mode needs start_pos and end_pos to be set' })
          }
        } else {
          errors.push({ path: basepath.concat([key]), message: 'supported modes are: lcut, lkeep, rcut, rkeep, char_pos' })
        }
      } else if (key === 'char_length') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else if (key === 'start_pos') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else if (key === 'end_pos') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
          'mode, char_length, start_pos, end_pos'
        })
      }
    }
  }
}
