export default Rename

function Rename (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((subElement, subIndex) => {
      if (typeof subElement === 'object') {
        if (!('search' in subElement && 'replace' in subElement)) {
          errors.push({ path: basepath.concat([subIndex]), message: 'Must have keys: search and replace' })
        }
        var keys = Object.keys(subElement)
        for (var index in keys) {
          var key = keys[index]
          if (key === 'search') {
            if (typeof subElement[key] !== 'string') {
              errors.push({ path: basepath.concat([subIndex, key]), message: key + ' must be a String' })
            }
          } else if (key === 'replace') {
            if (typeof subElement[key] !== 'string') {
              errors.push({ path: basepath.concat([subIndex, key]), message: key + ' must be a String' })
            }
          } else {
            errors.push({ path: basepath.concat([subIndex, key]), message: key + ' is not supported, supported values: search, replace' })
          }
        }
      } else {
        errors.push({ path: basepath.concat([subIndex]), message: 'Must be an Object' })
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'Must be an Array' })
  }
}
