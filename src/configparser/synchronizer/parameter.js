import Filters from '../filters'
import ConfigItem from './configitem'
import DefineAttribute from './defineattribute'
import FolderDefinition from './folderdefinition'
import WorkerConfig from './workerconfig'

export default Parameter

/*
const allowedSources = [
  "manual",
  "multi_job",
  "PRE_CHECKOUT",
  "PRE_CHECKIN"
]
*/

function Parameter (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    for (var index in keys) {
      var key = keys[index]
      if (key === 'define_attributes') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            DefineAttribute(subElement, errors, basepath.concat([key, subIndex]))
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'config_items') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            ConfigItem(subElement, errors, basepath.concat([key, subIndex]), element.allowed_sources)
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'allowed_method') {
        if (!(element[key] === 'single_object_start_and_single_object' || element[key] === 'wt_part_start_and_dependent_objects')) {
          errors.push({
            path: basepath.concat([key]),
            message: element[key] +
            ' is not supported, supported values: single_object_start_and_single_object, wt_part_start_and_dependent_objects'
          })
        }
      } else if (key === 'ignore_errors') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'filters') {
        Filters(element[key], errors, basepath.concat([key]))
      } else if (key === 'allowed_sources') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            /*
            if (!(subElement === 'manual' || subElement === 'multi_job' || )) {
              errors.push({
                path: basepath.concat([key, subIndex]),
                message: key +
                ' is not supported, supported values: manual, pre_check_in, post_check_in, multi_job'
              })
            }
            */
            // check if pre_check_in and folder_definition is present
            if (subElement === 'pre_check_in') {
              if ('config_items' in element) {
                element.config_items.forEach(subElement => {
                  if ('folder_definition' in subElement) {
                    errors.push({ path: basepath.concat([key]), message: 'pre_check_in cannot be used with folder definition' })
                  }
                })
              }
            }
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'folder_definitions') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            FolderDefinition(subElement, errors, basepath.concat([key, subIndex]))
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'worker_configs') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            WorkerConfig(subElement, errors, basepath.concat([key, subIndex]))
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
          'define_attributes, config_items, filters, allowed_sources, folder_definitions,' +
          'allowed_method, ignore_errors, worker_configs'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
