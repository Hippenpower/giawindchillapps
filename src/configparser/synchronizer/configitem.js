import Filters from '../filters'

export default ConfigItem

function ConfigItem (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    if (!(keys.includes('parameter_key') && keys.includes('new_values'))) {
      if (keys.includes('parameter_key')) {
        errors.push({ path: basepath.concat([]), message: 'missing key: new_values must be set' })
      } else if (keys.includes('new_values')) {
        errors.push({ path: basepath.concat([]), message: 'missing key: parameter_key must be set' })
      } else {
        errors.push({ path: basepath.concat([]), message: 'missing keys: parameter_key and new_values must be set' })
      }
    }
    for (var index in keys) {
      var key = keys[index]
      if (key === 'parameter_key') {
        if (!(element[key] === 'number' || element[key] === 'name' || element[key] === 'filename' || element[key] === 'iba_attribute')) {
          errors.push({ path: basepath.concat([key]), message: element[key] + ' is not supported, supported values: number, name, filename, iba_attribute' })
        }
        if (element[key] === 'iba_attribute') {
          if (!(keys.includes('iba_attribute') && keys.includes('force_write_iba'))) {
            errors.push({ path: basepath.concat([key]), message: 'please provide key: iba_attribute and force_write_iba' })
          }
        }
      } else if (key === 'iba_attribute') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'description') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'attribute_type') {
        if (!(element[key] === 'normal' || element[key] === 'enumerated_list')) {
          errors.push({ path: basepath.concat([key]), message: element[key] + ' is not supported, supported values: normal, enumerated_list' })
        }
      } else if (key === 'new_values') {
        if (Array.isArray(element[key])) {
          if (element[key].length === 0) {
            errors.push({ path: basepath.concat([key]), message: 'Array cannot be empty' })
          }
          element[key].forEach((subElement, subIndex) => {
            if (subElement.length === 0) {
              errors.push({ path: basepath.concat([key, subIndex]), message: subElement + ' help: use !{EXTERNAL_REFERENCE} or !{remove_if_next_empty}' })
            }
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'start_index') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else if (key === 'max_elements') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else if (key === 'ignore_if_more_than') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Integer' })
        }
      } else if (key === 'ignore_parametric_filename_standards') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'force_write_iba') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'execute_as_admin') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'visible_to_user') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'filters') {
        Filters(element[key], errors, basepath.concat([key]))
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
        'description, parameter_key, iba_attribute, new_values, start_index, max_elements, ' +
        'ignore_if_more_than, ignore_parametric_filename_standards, force_write_iba, execute_as_admin, ' +
        'visible_to_user, filters'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
