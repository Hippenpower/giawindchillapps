import PublishJob from './publishjob'
import Filters from '../filters'

export default WorkerConfig

function WorkerConfig (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    for (var index in keys) {
      var key = keys[index]
      if (key === 'description') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'publish_job') {
        PublishJob(element[key], errors, basepath.concat([key]))
      } else if (key === 'filters') {
        Filters(element[key], errors, basepath.concat([key]))
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
          'description, publish_job, filters'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
