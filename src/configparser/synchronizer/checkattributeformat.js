export default CheckAttributeFormat

function CheckAttributeFormat (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((subElement, subIndex) => {
      if (typeof subElement === 'object') {
        var keys = Object.keys(subElement)
        for (var index in keys) {
          var key = keys[index]
          if (key === 'mode') {
            if (subElement[key] === 'regex_match' || subElement[key] === 'regex_find') {
              if (!('regex_expression' in subElement)) {
                errors.push({ path: basepath.concat([subIndex, key]), message: 'regex_expression must be set' })
              }
            }
            if (!(subElement[key] === 'regex_match' || subElement[key] === 'regex_find' || subElement[key] === 'not_empty')) {
              errors.push({ path: basepath.concat([subIndex, key]), message: key + ' is not supported, supported values: regex_match, regex_find, not_empty' })
            }
          } else if (key === 'regex_expression') {
            if (typeof subElement[key] !== 'string') {
              errors.push({ path: basepath.concat([subIndex, key]), message: key + ' must be a String' })
            }
          } else {
            errors.push({ path: basepath.concat([subIndex, key]), message: key + ' is not supported, supported keys: mode, regex_expression' })
          }
        }
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'must be an Array' })
  }
}
