import Filters from '../filters'

export default FolderDefinition

function FolderDefinition (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    for (var index in keys) {
      var key = keys[index]
      if (key === 'folder_path') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            if (subElement.length === 0) {
              errors.push({ path: basepath.concat([key, subIndex]), message: subElement + ' help: use !{remove_if_next_empty}, !{force}' })
            }
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'default_folder_path') {
        if (Array.isArray(element[key])) {
          element[key].forEach((subElement, subIndex) => {
            if (subElement.length === 0) {
              errors.push({ path: basepath.concat([key, subIndex]), message: subElement + ' help: use !{remove_if_next_empty}, !{force}' })
            }
          })
        } else {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
        }
      } else if (key === 'folder_split_char') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'force_create') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Boolean' })
        }
      } else if (key === 'product_name') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'visible_to_user') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be an Boolean' })
        }
      } else if (key === 'filters') {
        Filters(element[key], errors, basepath.concat([key]))
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
        'folder_path, default_folder_path, folder_split_char, force_create, product_name, filters, ' +
        'visible_to_user'
        })
      }
    }
  }
}
