import Parameter from './parameter'
import FolderDefinition from './folderdefinition'

export default function checking (json) {
  const errors = []
  if (json && typeof json === 'object') {
    const firstLevelKeys = Object.keys(json)
    // config is mandatory
    for (var firstLevelIndex in firstLevelKeys) {
      var firstLevelKey = firstLevelKeys[firstLevelIndex]
      if (firstLevelKey === 'parameters') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((element, index) => {
            if (typeof element === 'object') {
              // var secondLevelKeys = Object.keys(element)
              Parameter(element, errors, [firstLevelKey, index])
            } else {
              errors.push({ path: [firstLevelKey, index], message: 'must be an Object' })
            }
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else if (firstLevelKey === 'global_folders') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((subElement, subIndex) => {
            FolderDefinition(subElement, errors, [firstLevelKey, subIndex])
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else {
        errors.push({ path: [firstLevelKey], message: firstLevelKey + ' is not supported, supported keys: parameters, global_folders' })
      }
    }
  }
  return errors
}
