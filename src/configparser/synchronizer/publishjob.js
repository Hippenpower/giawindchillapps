export default PublishJob

function PublishJob (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var keys = Object.keys(element)
    for (var index in keys) {
      var key = keys[index]
      if (key === 'queue_set') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'queue_priority') {
        if (!(element[key] === 'H' || element[key] === 'M' || element[key] === 'L')) {
          errors.push({ path: basepath.concat([key]), message: key + ' is not supported, supported values: H, M, L' })
        }
      } else if (key === 'publish_job_owner') {
        if (element[key].length === 0) {
          errors.push({ path: basepath.concat([key]), message: 'default is Administrator, use session for session user, epm_modifier, epm_creator' })
        }
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'viewable_link') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Boolean' })
        }
      } else if (key === 'force_republish') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Boolean' })
        }
      } else if (key === 'default_rep') {
        if (typeof element[key] !== 'boolean') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Boolean' })
        }
      } else if (key === 'rep_name') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'rep_description') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else if (key === 'structure_type') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Integer' })
        }
      } else if (key === 'job_source') {
        if (typeof element[key] !== 'number') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a Integer' })
        }
      } else if (key === 'job_info') {
        if (typeof element[key] !== 'string') {
          errors.push({ path: basepath.concat([key]), message: key + ' must be a String' })
        }
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: ' +
        'queue_set, queue_priority, publish_job_owner, viewable_link, force_republish, default_rep, ' +
        'rep_name, rep_description, structure_type, job_source, job_info'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
