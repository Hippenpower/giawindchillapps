import Filters from '../filters'
import Parameter from './parameter'

export default function checking (json) {
  const errors = []

  if (json && typeof json === 'object') {
    const firstLevelKeys = Object.keys(json)
    // config is mandatory
    if (firstLevelKeys.indexOf('config') === -1) {
      errors.push({ path: [], message: 'config must be set' })
    }
    for (var firstLevelIndex in firstLevelKeys) {
      var firstLevelKey = firstLevelKeys[firstLevelIndex]
      if (firstLevelKey === 'verbose') {
        if (typeof json[firstLevelKey] !== 'boolean') {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be a Boolean' })
        }
      } else if (firstLevelKey === 'config') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((element, index) => {
            if (typeof element === 'object') {
              var secondLevelKeys = Object.keys(element)
              if (secondLevelKeys.indexOf('parameter') === -1) {
                errors.push({ path: [firstLevelKey, index], message: 'parameter must be set' })
              }
              for (var secondLevelIndex in secondLevelKeys) {
                var secondLevelKey = secondLevelKeys[secondLevelIndex]
                if (secondLevelKey === 'filters') {
                  Filters(element[secondLevelKey], errors, [firstLevelKey, index, secondLevelKey])
                } else if (secondLevelKey === 'parameter') {
                  Parameter(element[secondLevelKey], errors, [firstLevelKey, index, secondLevelKey])
                } else {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey], message: secondLevelKey + ' is not supported, supported keys: filters, parameter' })
                }
              }
            } else {
              errors.push({ path: [firstLevelKey, index], message: 'must be an Object' })
            }
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else {
        errors.push({ path: [firstLevelKey], message: firstLevelKey + ' is not supported, supported keys: verbose, config' })
      }
    }
  }
  return errors
}
