export default Parameter

function Parameter (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var parameterKeys = Object.keys(element)
    for (var parameterIndex in parameterKeys) {
      var parameterKey = parameterKeys[parameterIndex]
      if (parameterKey === 'queue_set') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else if (parameterKey === 'queue_priority') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        } else {
          if (!(element[parameterKey] === 'M' || element[parameterKey] === 'L' || element[parameterKey] === 'H')) {
            errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' can only be L, M or H' })
          }
        }
      } else if (parameterKey === 'viewable_link') {
        if (typeof element[parameterKey] !== 'boolean') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a Boolean' })
        }
      } else if (parameterKey === 'force_republish') {
        if (typeof element[parameterKey] !== 'boolean') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a Boolean' })
        }
      } else if (parameterKey === 'default_rep') {
        if (typeof element[parameterKey] !== 'boolean') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a Boolean' })
        }
      } else if (parameterKey === 'rep_name') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else if (parameterKey === 'rep_description') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else if (parameterKey === 'structure_type') {
        if (typeof element[parameterKey] !== 'number') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a Integer' })
        }
      } else if (parameterKey === 'job_source') {
        if (typeof element[parameterKey] !== 'number') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a Integer' })
        }
      } else if (parameterKey === 'job_info') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else {
        errors.push({
          path: basepath.concat([parameterKey]),
          message: parameterKey + ' is not supported, supported keys: queue_set, queue_priority, ' +
            'force_republish, default_rep, rep_name, rep_description, structure_type, job_source, job_info'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
