import EMail from './email'
import RenameFix from './renamefix'
import Complex from './complexfolderstructure'
import CutVersion from './cutversion'
import CutString from './cutstring'
import NamingBase from './namingbase'

export default Parameters

function Parameters (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((parameterObject, parameterIndex) => {
      var parameterKeys = Object.keys(parameterObject)
      for (var parameterKeyIndex in parameterKeys) {
        var parameterKey = parameterKeys[parameterKeyIndex]
        if (parameterKey === 'logging') {
          if (typeof parameterObject[parameterKey] !== 'boolean') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a Boolean' })
          }
        } else if (parameterKey === 'write_path') {
          if (typeof parameterObject[parameterKey] !== 'boolean') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a Boolean' })
          }
        } else if (parameterKey === 'write_windchill') {
          if (typeof parameterObject[parameterKey] !== 'boolean') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a Boolean' })
          }
        } else if (parameterKey === 'add_revision') {
          if (typeof parameterObject[parameterKey] !== 'boolean') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a Boolean' })
          }
        } else if (parameterKey === 'make_zip') {
          if (typeof parameterObject[parameterKey] !== 'boolean') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a Boolean' })
          }
        } else if (parameterKey === 'occurrence_fix') {
          if (typeof parameterObject[parameterKey] !== 'string') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a String' })
          }
        } else if (parameterKey === 'output_path') {
          if (typeof parameterObject[parameterKey] !== 'string') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a String' })
          }
        } else if (parameterKey === 'add_revision_prefix') {
          if (typeof parameterObject[parameterKey] !== 'string') {
            errors.push({ path: basepath.concat([parameterIndex, parameterKey]), message: parameterKey + ' must be a String' })
          }
        } else if (parameterKey === 'naming_base') {
          NamingBase(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else if (parameterKey === 'complex_folder_structure') {
          Complex(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else if (parameterKey === 'cut_version') {
          CutVersion(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else if (parameterKey === 'cut_string') {
          CutString(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else if (parameterKey === 'rename_fix') {
          RenameFix(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else if (parameterKey === 'failed_jobs_emails') {
          EMail(parameterObject[parameterKey], errors, basepath.concat([parameterIndex, parameterKey]))
        } else {
          errors.push({
            path: basepath.concat([parameterIndex, parameterKey]),
            message: parameterKey + ' is not supported, supported keys: logging, write_path, ' +
            'write_windchill, add_revision, make_zip, occurrence_fix, output_path, add_revision_prefix, ' +
            'naming_base, complex_folder_structure, cut_version, cut_string, rename_fix, failed_jobs_emails'
          })
        }
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'must be an Array' })
  }
}
