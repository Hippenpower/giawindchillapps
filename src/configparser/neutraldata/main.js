import Filters from '../filters'
import Parameters from './parameters'

export default function checking (json) {
  const errors = []

  if (json && typeof json === 'object') {
    const firstLevelKeys = Object.keys(json)
    // config is mandatory
    if (firstLevelKeys.indexOf('output') === -1) {
      errors.push({ path: [], message: 'output must be set' })
    }
    for (var firstLevelIndex in firstLevelKeys) {
      var firstLevelKey = firstLevelKeys[firstLevelIndex]
      if (firstLevelKey === 'verbose') {
        if (typeof json[firstLevelKey] !== 'boolean') {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be a Boolean' })
        }
      } else if (firstLevelKey === 'log_file_path') {
        if (typeof json[firstLevelKey] !== 'string') {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be a String' })
        }
      } else if (firstLevelKey === 'output') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((element, index) => {
            if (typeof element === 'object') {
              var secondLevelKeys = Object.keys(element)
              if (secondLevelKeys.indexOf('parameters') === -1) {
                errors.push({ path: [firstLevelKey, index], message: 'parameters must be set' })
              }
              for (var secondLevelIndex in secondLevelKeys) {
                var secondLevelKey = secondLevelKeys[secondLevelIndex]
                if (secondLevelKey === 'filters') {
                  Filters(element[secondLevelKey], errors, [firstLevelKey, index, secondLevelKey])
                } else if (secondLevelKey === 'parameters') {
                  Parameters(element[secondLevelKey], errors, [firstLevelKey, index, secondLevelKey])
                }
              }
            } else {
              errors.push({ path: [firstLevelKey, index], message: 'must be an Object' })
            }
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + 'must be an Array' })
        }
      } else if (firstLevelKey === 'global_filters') {
        Filters(json[firstLevelKey], errors, [firstLevelKey])
      } else {
        errors.push({ path: [firstLevelKey], message: firstLevelKey + ' is not supported, supported keys: verbose, output, global_filters' })
      }
    }
  }
  return errors
}
