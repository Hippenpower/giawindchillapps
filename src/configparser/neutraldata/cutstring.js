export default RenameFix

function RenameFix (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var forthLevelKeys
    var forthLevelKey
    var forthLevelIndex

    forthLevelKeys = Object.keys(element)
    if (forthLevelKeys.toString().indexOf('end') === -1 && forthLevelKeys.toString().indexOf('start') === -1) {
      errors.push({ path: basepath.concat([]), message: 'must have either start or end key, or both' })
    }
    for (forthLevelIndex in forthLevelKeys) {
      forthLevelKey = forthLevelKeys[forthLevelIndex]
      if (forthLevelKey === 'int_start') {
        if (typeof element[forthLevelKey] !== 'number') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be an integer' })
        }
      } else if (forthLevelKey === 'first_start') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'last_start') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'first_regex_start') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'last_regex_start') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'int_end') {
        if (typeof element[forthLevelKey] !== 'number') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be an integer' })
        }
      } else if (forthLevelKey === 'first_end') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'last_end') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'first_regex_end') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      } else if (forthLevelKey === 'last_regex_end') {
        if (typeof element[forthLevelKey] !== 'string') {
          errors.push({ path: basepath.concat([forthLevelKey]), message: forthLevelKey + ' must be a string' })
        }
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'cut_string' + ' must be an Object' })
  }
}
