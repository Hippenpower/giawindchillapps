export default Complex

function Complex (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((complexObject, complexIndex) => {
      if (typeof complexObject !== 'string') {
        errors.push({ path: basepath.concat([complexIndex]), message: 'complex_folder_structure item must be a String' })
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'complex_folder_structure must be an Array' })
  }
}
