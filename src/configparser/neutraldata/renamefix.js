export default RenameFix

function RenameFix (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((RenameFixObject, RenameFixIndex) => {
      var RenameFixKeys = Object.keys(RenameFixObject)
      if (RenameFixKeys.length > 1) {
        errors.push({ path: basepath.concat([RenameFixIndex]), message: 'cannot have more than one element' })
      }
      for (var RenameFixKeyIndex in RenameFixKeys) {
        var RenameFixKey = RenameFixKeys[RenameFixKeyIndex]

        if (typeof RenameFixObject[RenameFixKey] !== 'string') {
          errors.push({ path: basepath.concat([RenameFixIndex, RenameFixKey]), message: RenameFixKey + ' must be a String' })
        }
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'RenameFix must be an Array' })
  }
}
