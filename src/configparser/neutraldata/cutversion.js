export default CutVersion

function CutVersion (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((cutVersionObject, cutVersionIndex) => {
      if (typeof cutVersionObject !== 'string') {
        errors.push({ path: basepath.concat([cutVersionIndex]), message: 'cut_version item must be a String' })
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'cut_version must be an Array' })
  }
}
