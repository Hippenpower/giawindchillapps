export default NamingBase

function NamingBase (element, errors, basepath = []) {
  if (typeof element === 'object') {
    if (!('base' in element)) {
      errors.push({
        path: basepath.concat([]),
        message: 'please provide key: base'
      })
    }
    var keys = Object.keys(element)
    for (var keyIndex in keys) {
      var key = keys[keyIndex]
      if (key === 'base') {
        if (typeof element[key] !== 'string') {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type string'
          })
        }
        if (element[key] === 'cad_name') {

        } else if (element[key] === 'epm_number') {

        } else if (element[key] === 'epm_name') {

        } else if (element[key] === 'cad_iba_attribute') {
          if (!('attribute' in element)) {
            errors.push({
              path: basepath.concat([]),
              message: 'please provide key: attribute, when using iba attribute'
            })
          }
        } else if (element[key] === 'master_iba_attribute') {
          if (!('attribute' in element)) {
            errors.push({
              path: basepath.concat([]),
              message: 'please provide key: attribute, when using iba attribute'
            })
          }
        } else if (element[key] === 'part_iba_attribute') {
          if (!('attribute' in element)) {
            errors.push({
              path: basepath.concat([]),
              message: 'please provide key: attribute, when using iba attribute'
            })
          }
        } else {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' is not supported, supported keys: cad_name, epm_number, epm_name, cad_iba_attribute, master_iba_attribute, part_iba_attribute'
          })
        }
      } else if (key === 'attribute') {
        if (typeof element[key] !== 'string') {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type string'
          })
        }
      } else if (key === 'locale') {
        if (typeof element[key] !== 'string') {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type string'
          })
        }
      } else if (key === 'approximation') {
        if (typeof element[key] !== 'string') {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type string'
          })
        } else {
          if (element[key] === 'levenshtein') {
          } else {
            errors.push({
              path: basepath.concat([key]),
              message: key + ' is not supported, supported keys: levenshtein'
            })
          }
        }
      } else if (key === 'priorities') {
        if (!Array.isArray(element[key])) {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type array'
          })
        } else {
          element[key].forEach((subelement, subindex) => {
            if (subelement === 'CADASSEMBLY') {
            } else {
              errors.push({
                path: basepath.concat([key, subindex]),
                message: key + ' is not supported, supported keys: CADASSEMBLY'
              })
            }
          })
        }
      } else if (key === 'ignore_fragments') {
        if (!Array.isArray(element[key])) {
          errors.push({
            path: basepath.concat([key]),
            message: key + ' must be of type array'
          })
        }
      } else {
        errors.push({
          path: basepath.concat([key]),
          message: key + ' is not supported, supported keys: base, attribute, locale, approximation, priorities, ignore_fragments'
        })
      }
    }
  } else {
    errors.push({
      path: basepath.concat([]),
      message: 'naming_base must be an Object'
    })
  }
}
