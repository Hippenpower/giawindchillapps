export default Filters

function Filters (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((subelement, subindex) => {
      if (typeof subelement === 'object') {
        var thirdLevelKeys = Object.keys(subelement)
        if (thirdLevelKeys.indexOf('filter_key') === -1) {
          errors.push({ path: basepath.concat([subindex]), message: 'filter_key must be set' })
        }
        for (var thirdLevelIndex in thirdLevelKeys) {
          var thirdLevelKey = thirdLevelKeys[thirdLevelIndex]
          if (thirdLevelKey === 'filter_key') {
            if (typeof subelement[thirdLevelKey] !== 'string') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an String' })
            }
            var filterKey = subelement[thirdLevelKey]
            if (filterKey === 'authoring_application') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'revision_prefix') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'file_extension') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'lifecycle_state') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'latest_version') {
              if (thirdLevelKeys.indexOf('filter_boolean') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_boolean must be set' })
              }
            } else if (filterKey === 'latest_iteration') {
              if (thirdLevelKeys.indexOf('filter_boolean') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_boolean must be set' })
              }
            } else if (filterKey === 'cad_iba_attributes') {
              if (thirdLevelKeys.indexOf('filter_attribute') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'if iba attribute is choosen, please provide filter_attribute' })
              }
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'part_iba_attributes') {
              if (thirdLevelKeys.indexOf('filter_attribute') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'if iba attribute is choosen, please provide filter_attribute' })
              }
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'product') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'indicator') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'organisation') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'queue_name') {
              if (thirdLevelKeys.indexOf('filter_values') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_values must be set' })
              }
            } else if (filterKey === 'is_epm_document') {
            } else if (filterKey === 'wtpart_epm_link_type') {
              if (thirdLevelKeys.indexOf('filter_reference') === -1) {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'filter_reference must be set' })
              }
            } else if (filterKey === 'is_wtpart') {
            } else if (filterKey === 'modify_date') {
              if (thirdLevelKeys.indexOf('filter_date_additional') === -1 && thirdLevelKeys.indexOf('filter_date') === -1) {
                errors.push({
                  path: basepath.concat([subindex, thirdLevelKey]),
                  message: 'if filter_date is choosen, please provide' +
                'filter_date_additional and filter_date'
                })
              }
            } else if (filterKey === 'number_of_parts') {

            } else {
              errors.push({
                path: basepath.concat([subindex, thirdLevelKey]),
                message: filterKey + ' is not supported, supported keys: authoring_application, revision_prefix, ' +
                'file_extension, lifecycle_state, latest_version, latest_iteration, cad_iba_attributes, ' +
                'part_iba_attributes, product, indicator, queue_name, organisation, modify_date, is_epm_document, wtpart_epm_link_type, is_wtpart'
              })
            }
          } else if (thirdLevelKey === 'filter_values') {
            if (Array.isArray(subelement[thirdLevelKey])) {
              subelement[thirdLevelKey].forEach((subsubelement, subsubindex) => {
                if (typeof subsubelement !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, subsubindex]), message: subsubelement + ' must be an String' })
                }
              })
            } else {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an Array' })
            }
          } else if (thirdLevelKey === 'filter_attribute') {
          } else if (thirdLevelKey === 'filter_boolean') {
            if (typeof subelement[thirdLevelKey] !== 'boolean') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a boolean' })
            }
          } else if (thirdLevelKey === 'filter_must_pass') {
            if (typeof subelement[thirdLevelKey] !== 'boolean') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a boolean' })
            }
          } else if (thirdLevelKey === 'filter_date') {
            if (typeof subelement[thirdLevelKey] !== 'string') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
            }
          } else if (thirdLevelKey === 'filter_reference') {
            if (typeof subelement[thirdLevelKey] !== 'string') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
            }
          } else if (thirdLevelKey === 'filter_date_additional') {
            if (typeof subelement[thirdLevelKey] !== 'string') {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
            }
          } else {
            errors.push({
              path: basepath.concat([subindex, thirdLevelKey]),
              message: thirdLevelKey + ' is not supported, supported keys: filter_values, filter_key, ' +
              'filter_attribute, filter_boolean, filter_must_pass, filter_date, filter_date_additional, ' +
              'filter_reference'
            })
          }
        }
      } else {
        errors.push({ path: basepath.concat([subindex]), message: 'must be an Object' })
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'filters' + ' must be an Array' })
  }
}
