import IntervalTime from './intervaltime'
import MonitorQueue from './monitorqueue'
import EMail from './email'

export default QueueNotification

function QueueNotification (element, errors, basepath = []) {
  if (!('queue_sets' in element && 'notification' in element)) {
    errors.push({ path: basepath.concat([]), message: 'must provide: queue_sets, notification' })
  }
  const levelKeys = Object.keys(element)
  for (var levelIndex in levelKeys) {
    var levelKey = levelKeys[levelIndex]
    if (levelKey === 'queue_sets') {
      if (Array.isArray(element[levelKey])) {
        element[levelKey].forEach((subelement, subindex) => {
          if (typeof element === 'object') {
            // var secondLevelKeys = Object.keys(element)
            MonitorQueue(subelement, errors, basepath.concat([levelKey, subindex]))
          } else {
            errors.push({ path: basepath.concat([levelKey, subindex]), message: 'must be an Object' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Array' })
      }
    } else if (levelKey === 'notification') {
      EMail(element[levelKey], errors, basepath.concat([levelKey]))
    } else if (levelKey === 'resend_email_interval') {
      if (typeof element[levelKey] !== 'object') {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Object' })
      } else {
        IntervalTime(element[levelKey], errors, basepath.concat([levelKey]))
      }
    } else {
      errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' is not supported, supported keys: queue_sets, notification, resend_email_interval' })
    }
  }
}
