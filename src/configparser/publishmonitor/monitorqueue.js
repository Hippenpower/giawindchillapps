export default MonitorQueue

function MonitorQueue (element, errors, basepath = []) {
  const levelKeys = Object.keys(element)
  for (var levelIndex in levelKeys) {
    var levelKey = levelKeys[levelIndex]
    if (levelKey === 'queue_name') {
      if (typeof element[levelKey] !== 'string') {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be a String' })
      }
    } else if (levelKey === 'limit') {
      if (element[levelKey] !== parseInt(element[levelKey])) {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Integer' })
      }
    } else if (levelKey === 'monitor_status') {
      if (!(element[levelKey] === 'NONEVENTFAILED' ||
        element[levelKey] === 'COMPLETED')) {
        errors.push({ path: basepath.concat([levelKey]), message: element[levelKey] + ' is not supported, supported values: NONEVENTFAILED, COMPLETED' })
      }
    } else {
      errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' is not supported, supported keys: queue_name, limit, monitor_status' })
    }
  }
}
