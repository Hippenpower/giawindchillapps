export default EMail

function EMail (element, errors, basepath = []) {
  if (Array.isArray(element)) {
    element.forEach((emailObject, emailIndex) => {
      var emailKeys = Object.keys(emailObject)
      for (var emailKeyIndex in emailKeys) {
        var emailKey = emailKeys[emailKeyIndex]
        if (emailKey === 'originator') {
          if (typeof emailObject[emailKey] !== 'string') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a String' })
          }
        } else if (emailKey === 'priority') {
          if (typeof emailObject[emailKey] !== 'boolean') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a Boolean' })
          }
        } else if (emailKey === 'receiver') {
          if (typeof emailObject[emailKey] !== 'string') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a String' })
          }
        } else if (emailKey === 'subject') {
          if (typeof emailObject[emailKey] !== 'string') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a String' })
          }
        } else if (emailKey === 'message_body') {
          if (typeof emailObject[emailKey] !== 'string') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a String' })
          }
        } else if (emailKey === 'message_body_file') {
          if (typeof emailObject[emailKey] !== 'string') {
            errors.push({ path: basepath.concat([emailIndex, emailKey]), message: emailKey + ' must be a String' })
          }
        } else {
          errors.push({
            path: basepath.concat([emailIndex, emailKey]),
            message: emailKey + ' is not supported, supported keys: originator, priority, ' +
            'receiver, subject, message_body, message_body_file'
          })
        }
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: 'email must be an Array' })
  }
}
