import IntervalTime from './intervaltime'
import WorkerNotification from './workernotification'
import QueueNotification from './queuenotification'

export default function checking (json) {
  const errors = []

  if (json && typeof json === 'object') {
    const firstLevelKeys = Object.keys(json)
    // config is mandatory
    for (var firstLevelIndex in firstLevelKeys) {
      var firstLevelKey = firstLevelKeys[firstLevelIndex]
      if (firstLevelKey === 'check_status_interval') {
        if (typeof json[firstLevelKey] !== 'object') {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Object' })
        } else {
          IntervalTime(json[firstLevelKey], errors, [firstLevelKey])
        }
      } else if (firstLevelKey === 'queue_notifications') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((element, index) => {
            if (typeof element === 'object') {
              // var secondLevelKeys = Object.keys(element)
              QueueNotification(element, errors, [firstLevelKey, index])
            } else {
              errors.push({ path: [firstLevelKey, index], message: 'must be an Object' })
            }
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else if (firstLevelKey === 'worker_notification') {
        if (Array.isArray(json[firstLevelKey])) {
          json[firstLevelKey].forEach((subelement, subindex) => {
            WorkerNotification(subelement, errors, [firstLevelKey, subindex])
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else {
        errors.push({ path: [firstLevelKey], message: firstLevelKey + ' is not supported, supported keys: check_status_interval, queue_notifications, worker_notification' })
      }
    }
  }
  return errors
}
