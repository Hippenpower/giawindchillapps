export default IntervalTime

function IntervalTime (element, errors, basepath = []) {
  const levelKeys = Object.keys(element)
  if (levelKeys.length === 0) {
    errors.push({ path: basepath.concat([]), message: 'provide at least one key, supported keys: time_format, check_interval' })
  }
  if (!isNaN(levelKeys[0]) && (function (x) { return (x | 0) === x })(parseFloat(levelKeys[0]))) {
    errors.push({ path: basepath.concat([]), message: 'provide an Object not an Array' })
  } else {
    for (var levelIndex in levelKeys) {
      var levelKey = levelKeys[levelIndex]
      if (levelKey === 'time_format') {
        if (!(element.levelKey === 'millis' ||
          element[levelKey] === 'seconds' ||
          element[levelKey] === 'minutes' ||
          element[levelKey] === 'hours' ||
          element[levelKey] === 'days')) {
          errors.push({ path: basepath.concat([levelKey]), message: element[levelKey] + ' is not supported, supported values: millis, seconds, minutes, hours, days' })
        }
      } else if (levelKey === 'check_interval') {
        if (element[levelKey] !== parseInt(element[levelKey])) {
          errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Integer' })
        }
      } else {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' is not supported, supported keys: time_format, check_interval' })
      }
    }
  }
}
