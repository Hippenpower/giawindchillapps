import IntervalTime from './intervaltime'
import EMail from './email'

export default WorkerNotification

function WorkerNotification (element, errors, basepath = []) {
  if (!('worker_names' in element && 'notification' in element && 'trigger_status_names' in element)) {
    errors.push({ path: basepath.concat([]), message: 'must provide: worker_names, notification, trigger_status_names' })
  }
  const levelKeys = Object.keys(element)
  for (var levelIndex in levelKeys) {
    var levelKey = levelKeys[levelIndex]
    if (levelKey === 'worker_names') {
      if (Array.isArray(element[levelKey])) {
        element[levelKey].forEach((subelement, subindex) => {
          if (typeof subelement !== 'string') {
            errors.push({ path: basepath.concat([levelKey, subindex]), message: subelement + ' must be a String' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Array' })
      }
    } else if (levelKey === 'notification') {
      EMail(element[levelKey], errors, basepath.concat([levelKey]))
    } else if (levelKey === 'trigger_status_names') {
      if (Array.isArray(element[levelKey])) {
        element[levelKey].forEach((subelement, subindex) => {
          if (!(subelement === 'FailstoStart' || subelement === 'On' || subelement === 'Off')) {
            errors.push({ path: basepath.concat([levelKey, subindex]), message: subelement + ' is not supported, supported values: FailstoStart, On, Off' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Array' })
      }
    } else if (levelKey === 'display') {
      if (!(element[levelKey] === 'all' || element[levelKey] === 'corrupt')) {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' is not supported, supported values: all, corrupt' })
      }
    } else if (levelKey === 'resend_email_interval') {
      if (typeof element[levelKey] !== 'object') {
        errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' must be an Object' })
      } else {
        IntervalTime(element[levelKey], errors, basepath.concat([levelKey]))
      }
    } else {
      errors.push({ path: basepath.concat([levelKey]), message: levelKey + ' is not supported, supported keys: worker_names, trigger_status_names, notification, resend_email_interval, display' })
    }
  }
}
