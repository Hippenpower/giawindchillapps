export default PromotionDefinition

function PromotionDefinition (element, errors, basepath = []) {
  const keys = Object.keys(element)
  for (var keyIndex in keys) {
    var key = keys[keyIndex]
    if (key === 'delimiter') {
      if (typeof element[key] !== 'string') {
        errors.push({ path: basepath.concat([key]), message: key + ' must be a string' })
      }
    } else if (key === 'allowed_promotion_state') {
      if (Array.isArray(element[key])) {
        element[key].forEach((subelement, subindex) => {
          if (subelement.length === 0) {
            errors.push({ path: basepath.concat([key, subindex]), message: 'supported standard values: approved, rejected (it may differ with workflow customization)' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
      }
    } else if (key === 'allowed_task_names') {
      if (Array.isArray(element[key])) {
        element[key].forEach((subelement, subindex) => {
          if (subelement.length === 0) {
            errors.push({ path: basepath.concat([key, subindex]), message: 'supported standard values: Approve Promotion Request (it may differ with workflow customization)' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
      }
    } else if (key === 'allowed_task_votes') {
      if (Array.isArray(element[key])) {
        element[key].forEach((subelement, subindex) => {
          if (subelement.length === 0) {
            errors.push({ path: basepath.concat([key, subindex]), message: 'supported standard values: [approve], [reject] (it may differ with workflow customization)' })
          }
        })
      } else {
        errors.push({ path: basepath.concat([key]), message: key + ' must be an Array' })
      }
    } else {
      errors.push({
        path: basepath.concat([key]),
        message: key +
        ' is not supported, supported keys: delimiter, allowed_promotion_state, allowed_task_names, allowed_task_votes'
      })
    }
  }
}
