import PromotionDefinition from './promotiondefinition'
export default Parameters

function Parameters (element, secondLevelKey, errors, basepath = []) {
  if (Array.isArray(element[secondLevelKey])) {
    element[secondLevelKey].forEach((subelement, subindex) => {
      var thirdLevelKeys = Object.keys(subelement)
      if (thirdLevelKeys.indexOf('parameter_key') === -1) {
        errors.push({ path: basepath.concat([subindex]), message: 'parameter_key must be set' })
      }
      for (var thirdLevelIndex in thirdLevelKeys) {
        var thirdLevelKey = thirdLevelKeys[thirdLevelIndex]
        if (thirdLevelKey === 'parameter_key') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + 'must be a string' })
          }
          if ((subelement[thirdLevelKey] === 'GIA_WM_CADDOC_IBA_ATTRIBUTE' ||
          subelement[thirdLevelKey] === 'GIA_WM_REF_MODEL_IBA_ATTRIBUTE' ||
          subelement[thirdLevelKey] === 'GIA_WM_PART_IBA_ATTRIBUTE') &&
          thirdLevelKeys.indexOf('iba_attribute') === -1) {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'when using IBA Attributes please provide iba_attribute' })
          }
        } else if (thirdLevelKey === 'iba_attribute') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'external_name') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'locale_lang') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'date_format') {
          if (subelement[thirdLevelKey].length === 0) {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'more info: https://docs.oracle.com/\njavase/6/docs/api/java/\ntext/SimpleDateFormat.html' })
          }
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'convert_float') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'parameter_rename') {
          if (Array.isArray(subelement[thirdLevelKey])) {
            subelement[thirdLevelKey].forEach((subsubsubelement, subsubsubindex) => {
              if (typeof subsubsubelement === 'object') {
              } else {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey, subsubsubindex]), message: 'must be an object' })
              }
            })
          } else {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an array' })
          }
        } else if (thirdLevelKey === 'allowed_date_formats') {
          if (Array.isArray(subelement[thirdLevelKey])) {
            subelement[thirdLevelKey].forEach((subsubsubelement, subsubsubindex) => {
              if (typeof subsubsubelement !== 'string') {
                errors.push({ path: basepath.concat([subindex, thirdLevelKey, subsubsubindex]), message: subsubsubelement + ' must be a string' })
              }
            })
          } else {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an array' })
          }
        } else if (thirdLevelKey === 'cut_string') {
          if (typeof subelement[thirdLevelKey] === 'object') {
            var forthLevelKeys = Object.keys(subelement[thirdLevelKey])
            if (forthLevelKeys.toString().indexOf('end') === -1 && forthLevelKeys.toString().indexOf('start') === -1) {
              errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: 'must have either start or end key, or both' })
            }
            for (var forthLevelIndex in forthLevelKeys) {
              var forthLevelKey = forthLevelKeys[forthLevelIndex]
              if (forthLevelKey === 'int_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'number') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be an integer' })
                }
              } else if (forthLevelKey === 'first_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'first_regex_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_regex_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'int_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'number') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be an integer' })
                }
              } else if (forthLevelKey === 'first_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'first_regex_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_regex_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]), message: forthLevelKey + ' must be a string' })
                }
              } else {
                errors.push({
                  path: basepath.concat([subindex, thirdLevelKey, forthLevelKey]),
                  message: forthLevelKey + ' is not supported, supported keys: int_start, first_start, last_start, first_regex_start,' +
                  ' last_regex_start, int_end, first_end, last_end, first_regex_end, last_regex_end'
                })
              }
            }
          } else {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an object' })
          }
        } else if (thirdLevelKey === 'promotion_definition') {
          if (typeof subelement[thirdLevelKey] === 'object') {
            PromotionDefinition(subelement[thirdLevelKey], errors, basepath.concat([subindex, thirdLevelKey]))
          } else {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' must be an object' })
          }
        } else if (thirdLevelKey === 'user_selection') {
          if (!(subelement[thirdLevelKey] === 'fullname' || subelement[thirdLevelKey] === 'name' || subelement[thirdLevelKey] === 'email' || subelement[thirdLevelKey] === 'firstname')) {
            errors.push({ path: basepath.concat([subindex, thirdLevelKey]), message: thirdLevelKey + ' is not supported, supported values: fullname, name, email, firstname' })
          }
        } else {
          errors.push({
            path: basepath.concat([subindex, thirdLevelKey]),
            message: thirdLevelKey +
            ' is not supported, supported keys: parameter_key, iba_attribute, external_name, locale_lang, date_format, convert_float, parameter_rename, cut_string, user_selection, promotion_definition'
          })
        }
      }
    })
  } else {
    errors.push({ path: basepath.concat([]), message: secondLevelKey + ' must be an Array' })
  }
}
