import Filters from '../filters'
import Parameters from './parameters'
import ParameterSnakes from './parameterSnakes'

export default function checking (json) {
  const errors = []

  if (json && typeof json === 'object') {
    const firstLevelKeys = Object.keys(json)
    // config is mandatory
    if (firstLevelKeys.indexOf('config') === -1) {
      errors.push({ path: [], message: 'config must be set' })
    }
    for (var firstLevelIndex in firstLevelKeys) {
      var firstLevelKey = firstLevelKeys[firstLevelIndex]
      if (firstLevelKey === 'verbose') {
        if (typeof json[firstLevelKey] !== 'boolean') {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Boolean' })
        }
      } else if (firstLevelKey === 'config') {
        if (Array.isArray(json[firstLevelKey])) {
          if (json[firstLevelKey].length === 0) {
            errors.push({ path: [firstLevelKey], message: 'please add an object' })
          }
          json[firstLevelKey].forEach((element, index) => {
            if (typeof element === 'object') {
              var secondLevelKeys = Object.keys(element)
              if (secondLevelKeys.indexOf('parameters') === -1 && secondLevelKeys.indexOf('parameter_snakes') === -1) {
                errors.push({ path: [firstLevelKey, index], message: 'parameters or parameter_snakes must be set' })
              }
              for (var secondLevelIndex in secondLevelKeys) {
                var secondLevelKey = secondLevelKeys[secondLevelIndex]
                // start filter (maybe take it seperate one day)
                if (secondLevelKey === 'filters') {
                  Filters(element[secondLevelKey], errors, [firstLevelKey, index, secondLevelKey])
                  // Parameters validity check
                } else if (secondLevelKey === 'parameters') {
                  Parameters(element, secondLevelKey, errors, [firstLevelKey, index, secondLevelKey])
                } else if (secondLevelKey === 'parameter_snakes') {
                  ParameterSnakes(firstLevelKey, index, secondLevelKey, element, errors)
                } else {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey], message: secondLevelKey + 'is not supported, supported keys: filters, parameters, parameter_snakes' })
                }
              }
            }
          })
        } else {
          errors.push({ path: [firstLevelKey], message: firstLevelKey + ' must be an Array' })
        }
      } else {
        errors.push({ path: [firstLevelKey], message: firstLevelKey + ' is not supported, supported keys: verbose, config' })
      }
    }
  } else {
    errors.push({ path: [], message: 'Config must be an Object' })
  }

  return errors
}
