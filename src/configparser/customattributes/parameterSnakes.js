import Parameters from './parameters'

export default ParameterSnakes

function ParameterSnakes (firstLevelKey, index, secondLevelKey, element, errors) {
  if (Array.isArray(element[secondLevelKey])) {
    element[secondLevelKey].forEach((subelement, subindex) => {
      var thirdLevelKeys = Object.keys(subelement)
      if (thirdLevelKeys.indexOf('parameter_key') === -1) {
        errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex], message: 'parameter_key must be set' })
      }
      for (var thirdLevelIndex in thirdLevelKeys) {
        var thirdLevelKey = thirdLevelKeys[thirdLevelIndex]
        if (thirdLevelKey === 'parameter_key') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: thirdLevelKey + 'must be a string' })
          }
        } else if (thirdLevelKey === 'external_name') {
          if (typeof subelement[thirdLevelKey] !== 'string') {
            errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: thirdLevelKey + ' must be a string' })
          }
        } else if (thirdLevelKey === 'parameter_values') {
          if (Array.isArray(subelement[thirdLevelKey])) {
            if (subelement[thirdLevelKey].length === 0) {
              errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: 'array must have one entry' })
            }
            subelement[thirdLevelKey].forEach((subsubelemet, subsubindex) => {
              if (typeof subsubelemet !== 'string') {
                errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, subsubindex], message: 'must be a string' })
              }
            })
          } else {
            errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: thirdLevelKey + ' must be an array' })
          }
        } else if (thirdLevelKey === 'parameters') {
          Parameters(subelement, thirdLevelKey, errors, [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey])
        } else if (thirdLevelKey === 'cut_string') {
          if (typeof subelement[thirdLevelKey] === 'object') {
            var forthLevelKeys = Object.keys(subelement[thirdLevelKey])
            if (forthLevelKeys.toString().indexOf('end') === -1 && forthLevelKeys.toString().indexOf('start') === -1) {
              errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: 'must have either start or end key, or both' })
            }
            for (var forthLevelIndex in forthLevelKeys) {
              var forthLevelKey = forthLevelKeys[forthLevelIndex]
              if (forthLevelKey === 'int_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'number') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be an integer' })
                }
              } else if (forthLevelKey === 'first_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'first_regex_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_regex_start') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'int_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'number') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be an integer' })
                }
              } else if (forthLevelKey === 'first_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'first_regex_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else if (forthLevelKey === 'last_regex_end') {
                if (typeof subelement[thirdLevelKey][forthLevelKey] !== 'string') {
                  errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey], message: forthLevelKey + ' must be a string' })
                }
              } else {
                errors.push({
                  path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey, forthLevelKey],
                  message: forthLevelKey + ' is not supported, supported keys: int_start, first_start, last_start, first_regex_start,' +
                  ' last_regex_start, int_end, first_end, last_end, first_regex_end, last_regex_end'
                })
              }
            }
          } else {
            errors.push({ path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey], message: thirdLevelKey + ' must be an object' })
          }
        } else {
          errors.push({
            path: [firstLevelKey, index, secondLevelKey, subindex, thirdLevelKey],
            message: thirdLevelKey +
            ' is not supported, supported keys: parameter_key, external_name, parameter_values, parameters, cut_string'
          })
        }
      }
    })
  } else {
    errors.push({ path: [firstLevelKey, index, secondLevelKey], message: secondLevelKey + ' must be an array' })
  }
}
