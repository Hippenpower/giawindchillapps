export default Parameter

function Parameter (element, errors, basepath = []) {
  if (typeof element === 'object') {
    var parameterKeys = Object.keys(element)
    for (var parameterIndex in parameterKeys) {
      var parameterKey = parameterKeys[parameterIndex]
      if (parameterKey === 'queue_set') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else if (parameterKey === 'queue_priority') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        } else {
          if (!(element[parameterKey] === 'M' || element[parameterKey] === 'L' || element[parameterKey] === 'H')) {
            errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' can only be L, M or H' })
          }
        }
      } else if (parameterKey === 'representation_name') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else if (parameterKey === 'representation_description') {
        if (typeof element[parameterKey] !== 'string') {
          errors.push({ path: basepath.concat([parameterKey]), message: parameterKey + ' must be a String' })
        }
      } else {
        errors.push({
          path: basepath.concat([parameterKey]),
          message: parameterKey + ' is not supported, supported keys: queue_set, queue_priority, ' +
            'representation_name, representation_description'
        })
      }
    }
  } else {
    errors.push({ path: basepath.concat([]), message: 'parameter must be an Object' })
  }
}
