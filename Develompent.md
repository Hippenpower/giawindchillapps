# Frontend
Use a decent vuejs ide (Mircosoft Visual Studio Code) to edit files

## Docker
To test build files, use make a docker image and run it locally (test build an chunk sizes etc.)

```bash
docker build -t giawindchillapps .
docker run -it -p 2000:80 --rm --name giawindchillapps giawindchillapps
```
